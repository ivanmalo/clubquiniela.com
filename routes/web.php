<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

\App::setLocale('es');

Route::get('/', function () 
{
    return redirect()->route('login');
});

Route::post('conekta/webhook', function() {
    $body = @file_get_contents('php://input');
    $data = json_decode($body);
    http_response_code(200); // Return 200 OK
    //if ($data->type == 'charge.paid'){
    if ($data->type){
      $payment_method = $data->payment_method->type;
      $msg = "Tu pago ha sido comprobado.";
      mail("client@email.com","Pago ". $payment_method ." confirmado",$msg);
    }
});

Route::get('/reglas',
        function () {
            return view('reglas');
})->name('reglas');

Route::get('/instructivo',function()
{
    return view('participantes/instructivo');
})->name('instructivo');

Route::get('/participantes/reglas', [
	    'uses'=>'ParticipantesController@juegoReglas',
	    'as'=>'participantes.juegoReglas',
]);

   // Authentication Routes...
    $router->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $router->post('login', 'Auth\LoginController@login');
    $router->post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    $router->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $router->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $router->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $router->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $router->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $router->post('password/reset', 'Auth\ResetPasswordController@reset');


Route::group(['middleware' => ['auth']], function() {
    
    /*    
    Route::get('/torneo-500', [
        'as'=>'torneo500',
        'uses' => 'HomeController@torneo500',
    ]);
    */
    
    //Agrega la relación a el torneo cuando se compra una entrada
    
    /*Ruta de paypal liga mx
    Route::get('/jvhzsdyiglGFHA8-w1231-8-torneo-apertura-2019', [
        'uses' => 'ParticipantesController@torneoClausuraPaypal',
        'as'=>'particioantes.torneoClausuraPaypal',
    ]);
    //Ruta de transferencia liga mx*/
    Route::post('/torneo-liga-mx', [
        'uses' => 'ParticipantesController@torneoClausuraTransferencia',
        'as'=>'particioantes.torneoClausuraTransferencia',
    ]);
    
    
    //Ruta de paypal liga mx revancha
    Route::get('/sdasfgfbdsdfFDGGR&W8-w12432fd-torneo-apertura-2019', [
        'uses' => 'ParticipantesController@torneoRevanchaPaypal',
        'as'=>'particioantes.torneoClausuraRevanchaPaypal',
    ]);
    //Ruta de transferencia liga mx revancha
    Route::post('/torneo--revancha-liga-mx-2019', [
        'uses' => 'ParticipantesController@torneoRevanchaTransferencia',
        'as'=>'particioantes.torneoRevanchaTransferencia',
    ]);
    
    
    
    Route::post('/storagefile','formController@storage');
    
    Route::put('/archvio/update/{id}',[
        'uses' => 'formController@update',
        'as'=>'archivos.update',
    ]);
    
    Route::get('/form/index',[
        'uses' => 'formController@index',
        'as'=>'archivos.index',
    ]);
    
    Route::get('/createfile','formController@create');
    
    
    Route::get('/home', [
        'as'=>'home',
        'uses' => 'HomeController@index',
    ]);
    
    Route::get('/participantes', [
        'as'=>'participantes',
        'uses' => 'HomeController@participantes',
    ]);
    
    
    
    //Juegos
        Route::get('/juegos2', [
        	    'as'=>'juegos.index2',
        	    'uses'=>'JuegosController@index2'
        ]);
        
        Route::resource('/juegos','JuegosController');
        
        
        Route::get('/participantes/perfil-usuario', [
                'uses' => 'ParticipantesController@userPerfil',
                'as'   => 'participantes.userPerfil'
        ]);     
        Route::get('/participantes/perfil-usuario/update', [
                'uses' => 'ParticipantesController@updatePass',
                'as'   => 'participantes.updatePass'
        ]);
        Route::get('/participantes/jugadores', [
                'uses' => 'ParticipantesController@juegoParticipantes',
                'as'   => 'participantes.juegoParticipantes'
        ]);
        
        //Seleccion y apuesta de equipos
        Route::post('/juegos/apostar', [
                'uses' => 'JuegosController@createEquipos',
                'as'   => 'juegos.createEquipos'
        ]);
        Route::get('/juegos/{id}/apostar/storeEquipo', [
                'uses' => 'JuegosController@storeEquipo',
                'as'   => 'juegos.storeEquipo'
        ]);
        
        /*
        Route::get('/participantes/comprar-entrada', [
                'uses' => 'ParticipantesController@comprarEntrada',
                'as'   => 'participantes.comprarEntrada'
        ]);

          
        Route::get('/participantes/comprar-entrada-revancha', [
                'uses' => 'ParticipantesController@comprarEntradaRevancha',
                'as'   => 'participantes.comprarEntradaRevancha'
        ]);
        */
        
    Route::group( ['middleware' => ['role:admin'] ], function() {
        
        //Administración
        Route::get('/administracion', [
                'uses' => 'AdministracionController@index',
                'as'   => 'administracion.index'
        ]);
        
        Route::post('/administracion/enviar', [
                'uses' => 'AdministracionController@sendMail',
                'as'   => 'administracion.sendMail'
        ]);
        
        Route::post('/administracion/enviarRecordatorio', [
                'uses' => 'AdministracionController@sendMailRecordatorio',
                'as'   => 'administracion.sendMailRecordatorio'
        ]);
        
        
        //Torneo
        Route::get('/torneo/storeTorneo', [
                'uses' => 'TorneoController@storeTorneo',
                'as'   => 'torneo.storeTorneo'
        ]);
        Route::get('/torneo/crearTorneo', [
                'uses' => 'TorneoController@crearTorneo',
                'as'   => 'torneo.crearTorneo'
        ]);
        Route::get('/torneo/principal', [
                'uses' => 'TorneoController@principal',
                'as'   => 'torneo.principal'
        ]);
        //jornadas
        Route::get('/torneo/{id}/jornadas/', [
                'uses' => 'TorneoController@torneoJornadas',
                'as'   => 'torneo.jornadas'
        ]);
        Route::get('/torneo/{id}/jornadas/create-jornada', [
                'uses' => 'TorneoController@createJornada',
                'as'   => 'torneo.createJornada'
        ]);
        Route::get('/torneo/{id}/jornadas/store-jornada', [
                'uses' => 'TorneoController@storeJornada',
                'as'   => 'torneo.storeJornada'
        ]);
        Route::get('/torneo/{id}/jornadas/delete-jornada', [
                'uses' => 'TorneoController@destroyJornada',
                'as'   => 'torneo.destroyJornada'
        ]);
        Route::get('/torneo/{id}/jornadas/update-jornada', [
                'uses' => 'TorneoController@updateJornada',
                'as'   => 'torneo.updateJornada'
        ]);
        //Partidos de jornada
        Route::get('/torneo/jornadas/{id}/edit-jornada', [
                'uses' => 'JuegosController@editJornada',
                'as'   => 'partidos.editJornada'
        ]);
        Route::get('/torneo/jornadas/partidos/{id}', [
                'uses' => 'JuegosController@jornadaPartidos',
                'as'   => 'partidos.jornadaPartidos'
        ]);
        Route::get('/torneo/jornadas/{id}/crear-partidos', [
                'uses' => 'JuegosController@createPartidos',
                'as'   => 'partidos.createPartidos'
        ]);
        Route::get('/torneo/jornadas/{id}/editPartido', [
                'uses' => 'JuegosController@editPartido',
                'as'   => 'partidos.editPartido'
        ]);
        Route::get('/torneo/jornadas/{id}/updateEquipo', [
                'uses' => 'JuegosController@updateEquipo',
                'as'   => 'partidos.updateEquipo'
        ]);
        Route::get('/torneo/jornadas/partidos/{id}/store-partidos', [
                'uses' => 'JuegosController@storePartido',
                'as'   => 'partidos.storePartido'
        ]);
        Route::get('/torneo/jornadas/partidos/{id}/delete-partidos/', [
                'uses' => 'JuegosController@eliminarPartido',
                'as'   => 'partidos.eliminarPartido'
        ]);
        //rutas de equipos
        Route::resource('/equipos','EquipoController');
        
        Route::get('/equipos/create/storeEquipo/', [
                'uses' => 'EquipoController@storeEquipo',
                'as'   => 'equipos.storeEquipo'
        ]);
        Route::get('/equipos/create/destroyEquipo/{id}', [
                'uses' => 'EquipoController@destroyEquipo',
                'as'   => 'equipos.destroyEquipo'
        ]);
        Route::get('/equipos/{id}/edit/updateEquipo', [
                'uses' => 'EquipoController@updateEquipo',
                'as'   => 'equipos.updateEquipo'
        ]);
        //enviar mails de vidas
        Route::get('/email-vidas/{id}', [
                'uses' => 'MailController@mailVidas',
                'as'   => 'emails.mailVidas'
        ]);
        
        Route::get('email', function() {
                return new \App\Mail\RecordatorioUser();
        });
        
        Route::get('emailvidas', function() {
                //\Illuminate\Support\Facades\Mail::to('miguel.perz@stetamalo.com')->send(new \App\Mail\VidasUser());
                return new \App\Mail\VidasUser();
        });
        
    
    });
    
});

Route::resource('reglas', 'ReglasController');
