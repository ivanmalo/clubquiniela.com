<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncuentroUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encuentro_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('encuentro_id');
            $table->string('apodo');
            $table->integer('equipo_seleccionado');
            $table->integer('resultado_equipo_local');
            $table->integer('resultado_equipo_visitante');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encuentro_user');
    }
}
