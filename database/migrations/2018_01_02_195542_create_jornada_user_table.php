<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJornadaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jornada_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id');
            $table->string('apodo');
            $table->integer('jornada_id');
            $table->integer('equipo_seleccionado');
            $table->string('resultado')->nullable();
            $table->string('equipo_logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jornada_user');
    }
}
