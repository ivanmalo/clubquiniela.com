<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTorneoUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('torneo_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('torneo_id');
            $table->integer('usuario_id');
            $table->string('apodo');
            $table->string('validado');
            $table->date('fecha_ingreso');
            $table->integer('vidas_restantes');
            $table->integer('archivo_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('torneo_usuario');
    }
}
