<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncuentroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encuentro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jornada_id');
            $table->integer('equipo_local_id');
            $table->integer('equipo_visitante_id');
            $table->integer('puntos_equipo_local');
            $table->integer('puntos_equipo_visitante');
            $table->date('fecha_inicio');
            $table->time('hora_inicio');
            $table->integer('numero_vidas');
            $table->integer('ganador')->default('0');
            $table->integer('perdedor')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encuentro');
    }
}
