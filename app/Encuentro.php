<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuentro extends Model
{
    //
    protected $table = 'encuentro';
    protected $guarded = ['id'];
    
    public function usuarios()
    {
        return $this->belongsToMany('App\User')->withPivot('apodo');
    }
    
    public function jornadas()
    {
        return $this->belongsTo('App\Jornada');
    }
    public function equipoLocal()
    {
        return $this->belongsTo('App\Equipo','equipo_local_id');
    }
    public function equipoVisitante()
    {
        return $this->belongsTo('App\Equipo','equipo_visitante_id');
    }
}
