<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Equipo;
use App\Jornada;
use App\EncuentroUsuario;
use App\JornadaUser;
use App\Torneo;
use Auth;

class jugadoresVidasres extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jugadores:vidasres';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jugadores que no seleccionaron equipo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
                //
        $torneos = Torneo::all();
        foreach($torneos as $torneo)
        {
            $jornada = $torneo->jornadaActiva->first();
            if($jornada->encuentros && $jornada->participacion > 0)
            {
                $jornada->participacion = 0;
                $jornada->save();
                $usuarios = $torneo->usuarios;
                foreach($usuarios as $usuario)
                {
                    $apodo = $usuario->pivot->apodo;
                    $vidas = $usuario->torneosPorId2($torneo->id, $apodo)->first();
                    $equipoSeleccionado = $usuario->pivot->equipo_seleccionado;
                    if($vidas)
                    {
                        if(is_null($equipoSeleccionado))
                        {
                            
                                $vidasnum = $vidas->pivot->vidas_restantes;
                                
                                $vidasRestantes = $vidasnum - 1;
                                
                                $torneoUsuario = \App\TorneoUsuario::where([ ['torneo_id', $torneo->id], ['usuario_id', $usuario->id], ['apodo', $apodo] ])->first();
                                
                                if($torneoUsuario)
                                {
                                    $torneoUsuario->vidas_restantes = $vidasRestantes;
                                    $torneoUsuario->save();
                                }
                                
                                $usuario_id = $usuario->id;
                                $equipo_logo = 'cancel.png';
                                $jornada->usuarios()->attach($usuario_id,['apodo'=>$apodo,'equipo_seleccionado'=>'0','resultado'=>'perder','equipo_logo'=>$equipo_logo]);
                        }
                    }
                    
                }
            }
        }
    }
}
