<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Equipo;
use App\Jornada;
use App\EncuentroUsuario;
use App\JornadaUser;
use App\Torneo;
use Auth;

class updateJornada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:jornada';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizar estado de la jornada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set("America/Mexico_City");
        $torneoUno = Torneo::find(1);
        $jornadaActiva = $torneoUno->jornadaActiva->first();
        $jornadaActiva->estado = null;
        $jornadaActiva->save();
        $ultimoEncuentro = $jornadaActiva->encuentros->last();
        $fechaTermino = $ultimoEncuentro->fecha_inicio;
        $jornadas = $torneoUno->jornadaTorneo;
        
        foreach($jornadas as $jornada){
            $encuentro = $jornada->encuentros->first();
            if($fechaTermino < $encuentro->fecha_inicio){
                $jornada->estado = 'en_juego';
                $jornada->save();
                $this->info('A iniciado una nueva Jornada de la liga!');
                break;
            }
        }
        
    }
}
