<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $table = 'roles';
    protected $guarded = ['id'];
    
    public function user($userID)
    {
         return $this->hasMany('App\User');
    }

}