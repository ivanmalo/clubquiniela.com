<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torneo extends Model
{
    //
    protected $table = 'torneo';
    protected $guarded = ['id'];
    
    
    
    public function usuariosApodos()
    {
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                                    ->withPivot('apodo','archivo_id');
    }
    
     public function usuarios()
    {
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                    ->withPivot('apodo','vidas_restantes')
                    ->orderBy('torneo_usuario.vidas_restantes', 'desc')
                    ->orderBy('torneo_usuario.apodo');
    }
    
    
/*    public function usuarios()
    {
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                    ->withPivot('apodo','vidas_restantes')
                    ->orderBy('torneo_usuario.vidas_restantes', 'desc')
                    ->orderBy('torneo_usuario.apodo');
    }*/
    public function usuarioVidas($id,$apodo)
    {
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                    ->withPivot('apodo','vidas_restantes')
                    ->wherePivot('torneo_id', $id)->wherePivot('apodo',$apodo);
    }
    
    public function misUsuarios($id)
    {
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                    ->withPivot('apodo','vidas_restantes')->withPivot('id')
                    ->wherePivot('usuario_id', $id);
    }
    
    public function jornadaTorneo()
    {
         return $this->hasMany('App\Jornada');
    }
    public function jornadaSeleccionada($jornada_id)
    {
        return $this->hasMany('App\Jornada')->where('id', $jornada_id);
    }
    public function jornadaActiva()
    {
         return $this->hasMany('App\Jornada')->where('estado', 'en_juego');
    }
    
    public function usuariosActivos(){
        return $this->belongsToMany('App\User', 'torneo_usuario', 'torneo_id', 'usuario_id')
                    ->withPivot('vidas_restantes')
                    ->wherePivot('vidas_restantes','>', 0)
                    ->distinct();
    }
    
}
