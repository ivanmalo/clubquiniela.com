<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;
use Image;

class EquipoController extends Controller
{
    //
    public function index(){
        $equipos = Equipo::all();
        return view('equipos.index',compact('equipos'));
    }
    
    public function show(){
        
    }
    
    public function create()
    {   
        return view('equipos.create');
    }
     
    public function store(Request $request){
        $nombre_img="";
        if($request->hasFile("escudo")) {
            
            $imagen = $request->file("escudo");
            $nombre_img = "escudo-".time().'.'.$imagen->getClientOriginalExtension();
            Image::make($imagen)->save(public_path('images/equipos/'.$nombre_img));
            $img = $nombre_img;
        }
        $equipos= new Equipo($request->except('escudo'));
        $equipos->escudo = $nombre_img;
        $equipos->equipo = $request->equipo;
        $equipos->save();
        //$equipo=Equipo::create($request->all());
        return  redirect()->route('equipos.index');
    }
    
    public function storeEquipo(Request $request){
        //$equipo=Equipo::create($request->all());
        return  redirect()->route('equipos.index');
    }
    
    public function edit($id){
        $equipo = Equipo::find($id);
        return view('equipos.edit',compact('equipo'));
    }
    public function update(Request $request, $id)
    {
      
            $nombre_img="";
            $img="";
            if($request->hasFile("escudo")) {
                
                $imagen = $request->file("escudo");
                $nombre_img = "escudo-".time().'.'.$imagen->getClientOriginalExtension();
                Image::make($imagen)->save(public_path('images/equipos/'.$nombre_img));
                $img = $nombre_img;
            }
            $equipo = Equipo::find($id);
            
            if($img != "") {
                if(!empty($equipo->escudo)) {
                    $fh = fopen(public_path('images/equipos/' . $equipo->escudo), 'a');
                    fwrite($fh, '');
                    fclose($fh);
                    unlink(public_path('images/equipos/' . $equipo->escudo));
                }
                $equipo->escudo = $img;
            }
             $equipo->update($request->except('escudo'));
             $equipo->equipo = $request->equipo;
             $equipo->save();
            //$equipo=Equipo::create($request->all());
            return  redirect()->route('equipos.index');
    }
    
    public function updateEquipo(Request $request, $id){
        $input = $request->all();
        $equipo = Equipo::find($id);
        if($equipo){
            
            $equipo->update($input);
            return  redirect()->route('equipos.index');
        }
        return "error";
    }
    
    public function destroyEquipo($id){
        $equipo = Equipo::find($id);
        if($equipo){
            if($equipo->escudo) {
                $imagen = $equipo->escudo;
                $fh = fopen(public_path('images/equipos/'.$imagen), 'a');
                fwrite($fh, '');
                fclose($fh);
                unlink(public_path('images/equipos/'.$imagen));
            }
            
            $equipo->delete();
            return  redirect()->route('equipos.index');
        }
        return "error";
    }
}
