<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Regla;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/participantes/comprar-entrada';
    protected $redirectTo = '/juegos';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'telefono'=>'required|digits:10',
            'fecha_nacimiento' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        //'cuenta'=>'required|max:20',
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        return User::create([
            'name' => $data['name'],
            'fecha_nacimiento'=>$data['fecha_nacimiento_submit'],
            'telefono'=>$data['telefono'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'como_se_entero'=>$data['como_se_entero'],
        ]);
        //'cuenta'=> $data['cuenta'],
    }
    
    
    public function showRegistrationForm()
    {
         $reglas =Regla::all();
        
        
        
        return view('auth.register',['reglas' => $reglas]); 
    }
    
}
