<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Torneo;
use App\Jornada;

class TorneoController extends Controller
{
    //
    public function crearTorneo(){
        return view('torneos.create');
    }
    
    public function principal(){
        $torneos = Torneo::all();
        return view('torneos.principal',compact('torneos'));
    }
    
    public function storeTorneo(Request $request)
	{
	    
		$this->validate($request, [
            'nombre' => 'required',
        ]);
        
        $torneo = Torneo::create($request->all());
        return redirect()->route('torneo.principal')
                        ->with('success','Nuevo Torneo creado');
	}
	public function show(Request $request){
	    
	}
	public function torneoJornadas(Request $request,$id){
	    
	    $torneo_id = $id;
	    
	    $jornadas = Jornada::where('torneo_id','=',$torneo_id)->get();
	    return view('torneos.jornadas',compact('jornadas','torneo_id'));
	}
	
	public function updateJornada(Request $request, $id){
	    
	    $jornada = Jornada::find($request->jornada_id);
	    
	    if($jornada) {
	        $jornada->clave_jornada = $request->clave_jornada;
	        $jornada->estado        = ($request->estado == "en_juego")? "en_juego" : null;
	        $jornada->participacion = ($request->participacion )? 1 : 0;
	        $jornada->save();
	        
	        return redirect()->action('TorneoController@torneoJornadas', [$id])
        					->with('success','La jornada se actualizó correctamente.');
	    }
	    
	    return back()->with('error','No se encontro la jornada seleccionada.');
	    /*
            if($request->participacion != 1) // 1 comprar entradas 0 cerrar entradas 
            {
                if($jornada->encuentros && $jornada->participacion > 0)
                {
                    $jornada->participacion = 0;
                    $jornada->save();
                    $usuarios = $torneo->usuarios;
                    
                    foreach($usuarios as $usuario)
                    {
                        $apodo = $usuario->pivot->apodo;
                        $vidas = $usuario->torneosPorId2($torneo->id, $apodo)->first();
                        $equipoSeleccionado = \App\JornadaUser::where([ ['jornada_id', $jornada->id], ['usuario_id', $usuario->id], ['apodo', $apodo] ])->first();
                        if($vidas->pivot->vidas_restantes > 0)
                        {
                            if(is_null($equipoSeleccionado))
                            {
                                $vidasnum = $vidas->pivot->vidas_restantes;
                                $vidasRestantes = $vidasnum - 1;
                                $torneoUsuario = \App\TorneoUsuario::where([ ['torneo_id', $torneo->id], ['usuario_id', $usuario->id], ['apodo', $apodo] ])->first();
                                
                                if($torneoUsuario)
                                {
                                    $torneoUsuario->vidas_restantes = $vidasRestantes;
                                    $torneoUsuario->save();
                                }
                                
                                $usuario_id = $usuario->id;
                                $equipo_logo = 'cancel.png';
                                $jornada->usuarios()->attach($usuario_id,['apodo'=>$apodo,'equipo_seleccionado'=>'0','resultado'=>'perder','equipo_logo'=>$equipo_logo]);
                            }   
                        }
                    }
                }
            }
            else if($request->participacion == 1)
            { 
             	$jornada->participacion=1;
             	$jornada->save();
            }
        */
	    
	}
	
	public function createJornada(Request $request,$id){
	    $torneo_id = $id;
	    return view('torneos.createJornada',compact('torneo_id'));
	}
	
	public function storeJornada(Request $request,$id)
	{
	   
	    
		$this->validate($request, [
            'clave_jornada' => 'required',
        ]);
        
        $torneo = Jornada::create($request->all());
        $torneo_id = $request->torneo_id;
	    $jornadas = Jornada::where('torneo_id','=',$torneo_id)->get();
        return redirect()->route('torneo.jornadas',[$id])
                        ->with('success','Nueva Jornada creada');
	}
	
	public function destroyJornada(Request $request,$id){
	   
	   $jornada = Jornada::find($request->id_jornada);
        if($jornada)
        {
            $jornada->delete();
            return redirect()->route('torneo.jornadas',[$id])
                        ->with('success','Jornada del torneo eliminada');
        }
        return "error";
	    
	}
}
