<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Encuentro;
use App\Equipo;
use App\Torneo;
use App\Regla;
use App\JornadaUser;

use App\Jornada;
use Auth;

class ParticipantesController extends Controller
{
    //
    public function index()
    {
       
    }
    

    public function show(Request $request)
    {
       
    }
    
    public function juegoReglas(Request $request)
    {
        $regla = Regla::find(2);
        return view('participantes.reglas', compact('regla'));
    }
    
    public function userPerfil(Request $request)
    {
         $usuario = Auth::user();
         $misTorneos = $usuario->torneosUnicos;
         $usuarioId = $usuario->id;
         return view('participantes.perfil',compact('usuario','misTorneos', 'usuarioId'));
    }
    
    public function updatePass(Request $request)
    { 
        
        $encry = password_hash($request->password, PASSWORD_DEFAULT);
        $usuario = User::find(Auth::user()->id);
        $usuario->password = $encry;
        $usuario->save();
        
        return redirect()->route('participantes.userPerfil');
    }
    
    public function juegoParticipantes(Request $request)
    {   
        
        $torneo = Torneo::find(1);
        $jornadaActiva = $torneo->jornadaActiva->first();
        $jornadas = $torneo->jornadaTorneo;
        
        return view('participantes.participantes',compact('jornadaActiva','jornadas'));            
    
        
    }
    
    
    public function comprarEntrada()
    {
        $usuario = Auth::user();
        $nombre = $usuario->name;
        $correo = $usuario->email;
        $torneoRevancha = $usuario->torneosPorId(11)->get()->count();
        
        return view('participantes.comprarEntrada', compact('nombre', 'correo', 'torneoRevancha') );
    }
    
    public function comprarEntradaRevancha()
    {
        $usuario = Auth::user();
        $nombre = $usuario->name;
        $correo = $usuario->email;
        $torneoRevancha = $usuario->torneosPorId(10)->get()->count();
        
        return view('participantes.comprarEntradaRevancha', compact('nombre', 'correo', 'torneoRevancha') );
    }
    
    //Inicio torneo normal
    public function  torneoClausuraPaypal(Request $request) {
        $usuario = Auth::user();
        $numero = $usuario->torneos->count();
        $apodo = $usuario->name;
        $torneoApertura = $usuario->torneosPorId(9)->get()->count();
        
        if( $torneoApertura < 3) {
            if($numero) {
                $apodo = $apodo." ".$numero;
            }
            $usuario->torneos()->attach(9, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 1 ]);
            return redirect()->route('juegos.index')
                            ->with('success', "Gracias por su compra.");
        }
        return back()->with('success', "Has superado el número máximo de entradas. Gracias por tu preferencia.");
    }
    
    public function  torneoClausuraTransferencia(Request $request) {
        $usuario = Auth::user();
        $numero = $usuario->torneos->count();
        $apodo = $usuario->name;
        $torneoApertura = $usuario->torneosPorId(11)->get()->count();
        if( $torneoApertura < 3) {    
            if($numero) {
                $apodo = $apodo." ".$numero;
            }
            $usuario->torneos()->attach(11, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 1 ]);
            return redirect()->route('juegos.index')
            ->with('success', "Su participación se validará inmediatamente, sin embargo el pago se debe hacer lo antes posible o su registro será dado de baja automáticamente.");
                        //->with('warning', "Su participación se validará inmediatamente, sin embargo el pago se debe hacer lo antes posible o su registro será dado de baja automáticamente.");
        }
        
        return back()->with('success', "Has superado el número máximo de entradas. Gracias por tu preferencia.");
    }
    //Fin torneo normal
    
    
    //Inicio torneo revancha
    public function  torneoRevanchaPaypal(Request $request) {
        $usuario = Auth::user();
        $numero = $usuario->torneos->count();
        $apodo = $usuario->name;
        $torneoApertura = $usuario->torneosPorId(11)->get()->count();
        
        if( $torneoApertura < 3) {
            
            if($numero) {
                $apodo = $apodo." ".$numero;
            }
            
            $usuario->torneos()->attach(11, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 1 ]);

            return redirect()->route('juegos.index')
                            ->with('success', "Gracias por su compra.");
        }
        
        return back()->with('success', "Has superado el número máximo de entradas. Gracias por tu preferencia.");
    }
    
    public function  torneoRevanchaTransferencia(Request $request) {
        $usuario = Auth::user();
        $numero = $usuario->torneos->count();
        $apodo = $usuario->name;
        $torneoApertura = $usuario->torneosPorId(10)->get()->count();
        if( $torneoApertura < 3) {    
            if($numero) {
                $apodo = $apodo." ".$numero;
            }
            
            $usuario->torneos()->attach(10, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 1 ]);
            return redirect()->route('juegos.index')
                        ->with('warning', "Su participación se validará inmediatamente, sin embargo el pago se debe hacer lo antes posible o su registro será dado de baja automáticamente.");
        }
        
        return back()->with('success', "Has superado el número máximo de entradas. Gracias por tu preferencia.");
    }
    //Fin torneo revancha

}
