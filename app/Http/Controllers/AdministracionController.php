<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;
use Mail;
use App\Mail\TestEmail;
use App\Mail\RecordatorioUser;
use App\TorneoUsuario;
use App\JornadaUser;
use App\Jornada;



class AdministracionController extends Controller
{
    
    public function index()
    {
        $usuario = Auth::user();
        $usuarios = User::has('torneos')->orderBy('name')->get();
        
        return view('administracion.envioManualCorreos', compact('usuarios'));
    }
    
    public function sendMail(Request $request)
    {
        
        $this->validate($request, [
            'usuario_id'          => 'required',
            'apodos'              => 'required',
        ]);
        
        $usuarioId = $request->usuario_id;
        $usuario = User::find($usuarioId);
        
        if($usuario){
            $correo = $usuario->email;
            $usuario->apodos = $request->apodos;
            
            $estatus = "success";
            $mensaje = "El correo se envio correctamente";
            try {
                Mail::to($correo)->send(new TestEmail($usuario));
            } 
            catch ( \Exception $e) {
                $estatus = "error";
                $mensaje = "El correo no se pudo enviar";
            }
            return redirect()->route('administracion.index')
                        ->with($estatus, $mensaje);
            
        }
        
        return redirect()->route('administracion.index')
                        ->with('error','No se encontro el usuario seleccionado');
    }
    
    public function sendMailRecordatorio(Request $request)
    {
        
        // $usuariosCorreo = collect();
        // $arrayJornadasActivas = Jornada::where('estado', 'en_juego')->get()->pluck('id')->toArray();
        // $usuariosActivos = TorneoUsuario::where('vidas_restantes','>', 0)
        //                                 ->with('usuario')
        //                                 ->with('torneo')
        //                                 ->get();
                                        
        // foreach($usuariosActivos as $usuarioActivo) {
        //     if($usuarioActivo->usuario) {
        //         $jornada = JornadaUser::where([ ['apodo', $usuarioActivo->apodo], ['usuario_id', $usuarioActivo->usuario_id] ])
        //                               ->orderBy('id','DEC')
        //                               ->first();
        //         if($jornada) {
        //             if( !in_array($jornada->jornada_id, $arrayJornadasActivas) ) {
        //                 $usuariosCorreo->push($usuarioActivo->usuario->email);
        //             }
        //         }
                
        //     }
        // }
        
        $correos  = User::all()->pluck('email')->toArray();
        // $correos = $usuariosCorreo->unique()->toArray();
        $estatus = "success";
        $numeroUsuarios = count($correos);
        $mensaje = "El correo se envio correctamente a ".$numeroUsuarios." usuarios.";
        if($numeroUsuarios){
            try {
                Mail::to($correos)->send(new RecordatorioUser());
            } 
            catch ( \Exception $e) {
                \Log::info($e);
                $estatus = "error";
                $mensaje = "El correo no se pudo enviar";
            }
            return redirect()->route('administracion.index')
                        ->with($estatus, $mensaje);
        }
        
        return redirect()->route('administracion.index')
                        ->with('error','No se encontro el usuario seleccionado');
    }
    
    
}
