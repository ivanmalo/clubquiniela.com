<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;
use App\Jornada;
use App\EncuentroUsuario;
use App\JornadaUser;
use App\Torneo;
use App\TorneoUsuario;
use Auth;

use App\Encuentro;

class JuegosController extends Controller
{
    
    
    public function index()
    {
        
        $fechaInicio = '2018-01-01';
        $horaInicio  = '00:00:00';
        $fechaInicio2 = '2018-01-01';
        $horaInicio2  = '00:00:00';
        date_default_timezone_set("America/Mexico_City");
        $fecha_entrada = date ("Y-m-d H:i:s", time());
        $adminTorneo = Torneo::all();
        $usuario = Auth::user();
        $usuarioId = $usuario->id;
        $misTorneos = $usuario->torneosDiferentes;
        $sinApuesta = array();
        $jornadaActiva = array();
        //Solución temporal, cambiar por algo bien.
        $torneoUno = Torneo::find(5);
        $torneoDos = Torneo::find(6);
        
        if($torneoUno) {
            $jornadaActiva = $torneoUno->jornadaActiva->first();
            
            if($jornadaActiva) {
                $primerEncuentro = $jornadaActiva->encuentros->first();
                
                if($primerEncuentro) {
                    $horaPartido = $primerEncuentro->hora_inicio;
                    $arrayHora = explode(":", $horaPartido);
                    $transcurridoMinutos = intval( $arrayHora[1] ) - 30;
                    
                    if($transcurridoMinutos < 0)
                    {
                        $arrayHora[0] = intval( $arrayHora[0] ) - 1;
                        $arrayHora[1] = 60 + intval($transcurridoMinutos);
                    }
                  
                    if( count( $arrayHora[0] ) < 2 ) 
                    {
                        $arrayHora[0] = "0"+$arrayHora[0];
                    }
                    
                    $horaParticipacion = implode(":", $arrayHora);
                    $horaInicio = $horaParticipacion;
                    $fechaInicio  = $primerEncuentro->fecha_inicio;
                }
            }
        }
        
        if($torneoDos) {
            $jornadaActiva2 = $torneoDos->jornadaActiva->first();
            
            if($jornadaActiva2) {
                $segundoEncuentro = $jornadaActiva2->encuentros->first();
                
                if($segundoEncuentro) {
                    $horaPartido2 = $segundoEncuentro->hora_inicio;
                    $arrayHora2 = explode(":", $horaPartido2);
                    $transcurridoMinutos2 = intval( $arrayHora2[1] ) - 30;
                    
                    if($transcurridoMinutos2 < 0)
                    {
                        $arrayHora2[0] = intval( $arrayHora2[0] ) - 1;
                        $arrayHora2[1] = 60 + intval($transcurridoMinutos2);
                    }
                  
                    if( count( $arrayHora2[0] ) < 2 ) 
                    {
                        $arrayHora2[0] = "0"+$arrayHora2[0];
                    }
                    
                    $horaParticipacion2 = implode(":", $arrayHora2);
                    $horaInicio2 = $horaParticipacion2;
                    $fechaInicio2  = $segundoEncuentro->fecha_inicio;
                }
            }
        }
        
        foreach($misTorneos as $torneo){
            $jornada = $torneo->jornadaActiva->first();
            $misUsuarios = $torneo->misUsuarios($usuarioId)->get();
            foreach($misUsuarios as $usuarioApodo){
                $apodo = $usuarioApodo->pivot->apodo;
                if( !is_null($jornada) )
                    {
                    $jornadaActiva[$torneo->id] = $jornada->participacion;
                    $validacion = $usuario->jornadaValidacion($apodo, $jornada->id)->get()->first();
                    if(is_null($validacion)){
                        $sinApuesta[] = $apodo;
                    }
                }
            }
        }
        
        return view('juegos.index',compact('misTorneos', 'usuarioId', 'fecha_entrada','jornadaActiva',
                                            'sinApuesta', 'partidos', 'fechaInicio', 'horaInicio',
                                            'fechaInicio2', 'horaInicio2', 'adminTorneo'));
    }
    
    
    
    
    
    
    
    
    
    // public function index()
    // {
    //     $usuario = Auth::user();
    //     $torneoCuantro = Torneo::find(4); 
    //     $torneo4 = TorneoUsuario::where('torneo_id',4)->with('archivo')->get();
        
    //     return view('juegos.index',compact('torneo4'));
    // }
    
    public function index2()
    {
        date_default_timezone_set("America/Mexico_City");
        $usuario = Auth::user();
        $torneoCuantro = Torneo::find(4);
        $torneo4 = TorneoUsuario::where('torneo_id',4)->with('archivo')->get();
        return view('juegos.index2', compact('torneo4'));
    }
    
    public function createEquipos(Request $request)
    {   
        $this->validate($request, [
            'apodo' => 'required',
        ]);
        
        $usuario = Auth::user();
        $torneo_id = $request->torneo_id;
        $torneo = Torneo::find($torneo_id);
        $partidos = null; 
        if($torneo) {
            $partidos = $torneo->jornadaActiva->first();
        }
        
        $apodos = $request->apodo;
        $apodo = explode("-", $apodos);
        $jornada = Jornada::find($partidos->id);
        $encuentros = null;
        if($jornada){
           $encuentros = $jornada->encuentros;
        }
        
        $usuarioId = $apodo[0];
        $apodo = $apodo[1];
        
        $misJornadas = $usuario->jornadasApodo($apodo)->get();
        $misJornadas = $misJornadas->where('torneo_id', $torneo_id);
        $equiposUsados = array();
        foreach($misJornadas as $miJornada){
            $equiposUsados[] = $miJornada->pivot->equipo_seleccionado;
        }
       // $jornada_user = JornadaUser::where('usuario_id','=',$user_id)->where('jornada_id','=',$jornada_id)->get();
        return view('partidos.equipoJornada',compact('torneo', 'encuentros','usuarioId','jornada','apodo', 'equiposUsados'));
    }
    
    public function storeEquipo(Request $request,$id)
    {
        $usuario_id = $request->usuario_id;
        $equipo_seleccionado = $request->equipo_seleccionado;
        $apodo = $request->apodo;
        $equipo_logo = $request->equipo_logo;
        $jornada = Jornada::find($id);
        $jornada->usuarios()->attach($usuario_id,['apodo'=>$apodo,'equipo_seleccionado'=>$equipo_seleccionado,'equipo_logo'=>$equipo_logo]);
        return redirect()->route('juegos.index');
    }

    //End seccion de juegos en usuario
    public function show(Request $request)
    {
        
    }
    public function editJornada(Request $request,$id)
    {
        $torneo_id = $id;
        $jornada = Jornada::find($request->jornada_id);
        return view('torneos.editJornada',compact('jornada','torneo_id'));
    }
    public function updateJornada(Request $request,$id)
    {
        $jornada = Jornada::find($id);
    }
    
    public function jornadaPartidos(Request $request, $id){
        
        //Equipos en los encuentros
        $torneo = Torneo::find($id);
        $jornada = $torneo->jornadaSeleccionada($request->jornada_id)->first();
        $encuentros = $jornada->encuentros;
        
        return view('partidos.index',compact('encuentros','jornada','torneo'));
    }
    
    public function createPartidos(Request $request, $id){
        $torneo = Torneo::find($id);
        $jornada = $torneo->jornadaSeleccionada($request->jornada_id)->first();
        $equipos = Equipo::all();
        
        return view('partidos.create',compact('jornada','torneo','equipos'));
    }
    
    public function storePartido(Request $request, $id){
        
        
        $partido = Encuentro::create($request->except('torneo_id'));
        $torneo = Torneo::find($request->torneo_id);
        $jornada = $torneo->jornadaSeleccionada($request->jornada_id)->first();
        $encuentros = $jornada->encuentros;
        
        return view('partidos.index',compact('encuentros','jornada','torneo'));
    }
    
    public function editPartido(Request $request, $id){
        $torneo_id = $request->torneo_id;
        
        $tipo = $request->tipo;
        $jornada = Jornada::find($id);
        $encuentros = $jornada->encuentros;
        $partido = $jornada->encuentros->where('id',$request->id_partido)->first();
      
        return view('partidos.edit',compact('jornada','partido','encuentros','tipo','torneo_id'));
    }
    
    public function updateEquipo(Request $request, $id)
    {
        if($request->tipo == "partido")
        {
            
            $torneo = Torneo::find($request->torneo_id);
            
            $jornada = $torneo->jornadaActiva->first();
            
            $partido = Encuentro::find($request->partido_id);
            $partido->update($request->except('partido_id','tipo','torneo_id'));
        }
        if($request->tipo == "puntos")
        {
            $partido = Encuentro::find($request->partido_id);
            $partido->puntos_equipo_local = ($request->puntos_equipo_local)? intval($request->puntos_equipo_local) : 0 ;
            $partido->puntos_equipo_visitante = ($request->puntos_equipo_visitante)? intval($request->puntos_equipo_visitante) : 0 ;
        
            if($partido->puntos_equipo_local > $partido->puntos_equipo_visitante)
            {
                $partido->ganador = $request->equipo_local_id;
                $partido->perdedor = $request->equipo_visitante_id;
            }
            elseif($partido->puntos_equipo_visitante > $partido->puntos_equipo_local)
            {
                $partido->ganador = $request->equipo_visitante_id;
                $partido->perdedor = $request->equipo_local_id;
            }
            else{
                $partido->ganador  = 0;
                $partido->perdedor = 0;
            }
            $partido->save();
            
            //Cambiar por el torneo actual
            //!!!!!!!!!!!!!!!!!!!!!!!!
            $torneo = Torneo::find($request->torneo_id);
            
            
                
                $jornada = $torneo->jornadaActiva->first();
                
                $usuarios = $jornada->usuarios;
                
                foreach($usuarios as $user)
                {
                    
                    $apodo = $user->pivot->apodo;
                    $vidas = $user->torneosPorId2($torneo->id, $apodo)->first();
                    $equipoSeleccionado = $user->pivot->equipo_seleccionado; 
                    
                    if($vidas) {
                         
                        $vidasnum = $vidas->pivot->vidas_restantes;
                        
                        if( $equipoSeleccionado == $partido->equipo_local_id || $equipoSeleccionado == $partido->equipo_visitante_id) {
                           
                            
                            if( $equipoSeleccionado == $partido->ganador || ($partido->ganador == 0 && $partido->perdedor == 0) )
                            {
                                
                                $jornadaUsuario = \App\JornadaUser::where([ ['jornada_id', $jornada->id], ['usuario_id', $user->id], ['apodo', $apodo] ])->first();
                                if($jornadaUsuario){
                                    $jornadaUsuario->resultado = "ganar";
                                    $jornadaUsuario->save();
                                }
                                
                            }
                            elseif($equipoSeleccionado == $partido->perdedor)
                            {
                                
                                $vidasRestantes = $vidasnum - 1;
                                
                                $torneoUsuario = \App\TorneoUsuario::where([ ['torneo_id', $torneo->id], ['usuario_id', $user->id], ['apodo', $apodo] ])->first();
                                
                                
                                if($torneoUsuario){
                                    $torneoUsuario->vidas_restantes = $vidasRestantes;
                                    $torneoUsuario->save();
                                }
                                
                                $jornadaUsuario = \App\JornadaUser::where([ ['jornada_id', $jornada->id], ['usuario_id', $user->id], ['apodo', $apodo] ])->first();
                                
                                if($jornadaUsuario){
                                    $jornadaUsuario->resultado = "perder";
                                    $jornadaUsuario->save();
                                }
                                
                            }
                        }
                    }    
                }
            
        }
            
            $encuentros = $jornada->encuentros;
            
            return view('partidos.index',compact('encuentros','jornada','torneo'));
        // return redirect()->route('partidos.jornadaPartidos',[$torneo->id])
        //                 ->with('success','Partido Editado');
    }
    
    public function eliminarPartido(Request $request, $id){
        $partido = Encuentro::find($request->id_partido);
        if($partido)
        {
            $partido->delete();
            
            $torneo = Torneo::find($id);
            $jornada = $torneo->jornadaSeleccionada($request->jornada_id)->first();
            $encuentros = $jornada->encuentros;
            
            return view('partidos.index',compact('encuentros','jornada','torneo'));
        }
        return "error";
    }
    
    
}
