<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Torneo;
use \Mail;
use \App\Mail\TestEmail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return redirect()->route('juegos.index');
    }
    

    public function participantes()
    {
      $usuario = Auth::user();
      
      $torneos = $usuario->torneos->unique('id');
      return view('participantes', compact('torneos', 'usuario'));
    }
    
    public function conekta(){
        require_once base_path('vendor/conekta/conekta-php/lib/Conekta.php');
        \Conekta\Conekta::setApiKey("key_xbj61PxqctXUoVo3tfP8rw");
        \Conekta\Conekta::setApiVersion("2.0.0");
        $order = null;
        try{
          $order = \Conekta\Order::create(
            array(
              "line_items" => array(
                array(//product
                  "name" => "Torneo Amateur Prueba",
                  "unit_price" => 25000,
                  "quantity" => 1
                )
              ),//customer_info
              "currency" => "MXN",
              "customer_info" => array(
                "name" => "El José",
                "email" => "jose@stetamalo.com",
                "phone" => "+524499503433"
              ),
              "charges" => array(
                  array(
                      "payment_method" => array(
                        "type" => "oxxo_cash"
                      )//payment_method
                  ) //first charge
              ) //charges
            )//order
          );
        } catch (\Conekta\ParameterValidationError $error){
          dd( $error->getMessage() );
        } catch (\Conekta\Handler $error){
          dd( $error->getMessage() );
        }
        
    }
    
    
    public function torneoRevancha() {
      $usuario = Auth::user();
      $numero = $usuario->torneos->count();
      $apodo = $usuario->name;
      
      $torneoRevancha = $usuario->torneosPorId(3)->get()->count();
      if( $torneoRevancha < 3){
        if($numero){
          $apodo = $apodo." ".$numero;
        }
        $usuario->torneos()->attach(3, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 1 ]);
      }  
      return redirect()->route('juegos.index');
    }
    
    public function torneo500() {
      $usuario = Auth::user();
      $numero = $usuario->torneos->count();
      $apodo = $usuario->name;
      if($numero){
        $apodo = $apodo." ".$numero;
      }
      $usuario->torneos()->attach(2, ['apodo' => $apodo, 'validado' => 'pendiente', 'fecha_ingreso' => date('Y-m-d'), 'vidas_restantes' => 2 ]);
      return redirect()->route('juegos.index');
    }
}
