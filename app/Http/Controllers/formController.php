<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Auth;
use \App\Archivo;
class formController extends Controller
{
    public function index()
    {
        $usuario = Auth::user();
        
        $participaciones = $usuario->torneosPorId(4)->get();
        $indice = 1;
        
        foreach($participaciones as $participacion) {
            $participacion->archivo = archivo($participacion->pivot->id);
            
        }
        
        
        
        return view('form/index', compact('participaciones', 'indice'));
    }
    
    public function storage( Request $request)
    {
        
        $this->validate($request, [
            'apodo'         => 'required',
            'torneo_id'         => 'required',
            'file'         => 'required',
        ]);
        
        $excel = $request->file('file');
        $archivo ='/storage/'. $excel->store('files','public');
        $linkfile = asset($archivo);
        $newRegister = new archivo;
        $newRegister->apodo        =  $request->apodo;
        $newRegister->ruta         =  $linkfile;
        $newRegister->fecha_subida =  date('Y-m-d');
        $newRegister->torneo_id    =  $request->torneo_id;
        $newRegister->user_id      =  Auth::user()->id;
        $newRegister->save();
      
    //   $result = archivo::create(
    //       [
    //           'apodo' => 'un apodo',
    //           'ruta'  => $linkfile,
    //           'fecha_subida' => date('Y-m-d'),
    //           'torneo_id' => 1,
    //           'user_id' => Auth::user()->id,
               
    //       ]
    //       );
    
      return redirect()->route('archivos.index')
                    ->with('success',"Su archivo se ha guardado correctamente.");
    }
    
    
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'apodo'         => 'required',
            'torneo_id'         => 'required',
        ]);
        
        $archivo = Archivo::find($id);
        
        if($archivo) {
            
            if( $request->archivo ) {
                $excel = $request->file('archivo');
                $archivoE ='/storage/'. $excel->store('files','public');
                $linkfile = asset($archivoE);
                $archivo->ruta =  $linkfile;
            }
            $archivo->apodo        =  $request->apodo;
            $archivo->fecha_subida =  date('Y-m-d');
            $archivo->save();
        }
      
        return redirect()->route('archivos.index')
                    ->with('success',"Su archivo se ha guardado correctamente.");
    }
    
    
    public function create()
    {
        return view('form/create');
    }
}
