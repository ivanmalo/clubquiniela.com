<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Equipo;
use App\Jornada;
use App\EncuentroUsuario;
use App\JornadaUser;
use App\Torneo;
use Auth;

class MailController extends Controller
{
    //
    public function mailVidas($id)
    {
        $torneo = Torneo::find($id);
        $jornada = $torneo->jornadaActiva->first();
        $usuarios = $torneo->usuarios;
        $usuarios = $usuarios->unique('id');
        
        
        foreach ($usuarios as $user) {
            $apodos="";
            $apodo = $user->pivot->apodo;
            $vidas = $user->torneosPorId2($torneo->id, $apodo)->first();
           if($vidas->pivot->vidas_restantes > 0){
            $jornadaUsuario = JornadaUser::where([ ['jornada_id', $jornada->id], ['usuario_id', $user->id],['resultado','perder'] ])->orderBy('apodo', 'asc')->get();
            
            if(count($jornadaUsuario)>0)
            {
                
                $userapodo = $jornadaUsuario->pluck('apodo');
                foreach($userapodo as $mailuser)
                {
                    $apodos.=$mailuser.", ";
                }
                
            
            
                 Mail::to($user->email)->send(new \App\Mail\VidasUser($user->email,$user->name,$apodos,$jornada->clave_jornada,$torneo->nombre));
                 
                 
             sleep(1);
            }
           }
           
                    
               
        }
        return redirect()->route('torneo.jornadas',[$torneo->id])
                        ->with('success','Emails enviados');
    }
}
