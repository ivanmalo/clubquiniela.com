<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use \App\Notifications\CustomResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','fecha_nacimiento','telefono','cuenta','email', 'password','como_se_entero',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function sendPasswordResetNotification($token)
    {
       $this->notify(new CustomResetPasswordNotification($token));
    }
    
    public function archivos()
    {
        return $this->belongsToMany('App\Archivo');
    }
    
    public function encuentros()
    {
        return $this->belongsToMany('App\Encuentro');
    }
    public function encuentrosDos()
    {
        return $this->belongsToMany('App\Encuentro')
                    ->withPivot('apodo', 'equipo_seleccionado');
    }
    public function encuentrosApodo($apodo)
    {
        return $this->belongsToMany('App\Encuentro')
                    ->withPivot('apodo', 'equipo_seleccionado')
                    ->wherePivot('apodo', $apodo);
    }
    
    public function torneos()
    {
        return $this->belongsToMany('App\Torneo', 'torneo_usuario', 'usuario_id', 'torneo_id');
    }
    
    public function torneosDiferentes()
    {
        return $this->belongsToMany('App\Torneo', 'torneo_usuario', 'usuario_id', 'torneo_id')
                    ->where('fecha_final','>',Date('Y-m-d'))
                    ->distinct();
    }
    public function torneosUnicos()
    {
        return $this->belongsToMany('App\Torneo', 'torneo_usuario', 'usuario_id', 'torneo_id')
                    ->distinct();
    }
    
    public function torneosPorId($id)
    {
        return $this->belongsToMany('App\Torneo', 'torneo_usuario', 'usuario_id', 'torneo_id')
                    ->withPivot('id','torneo_id','vidas_restantes','apodo')
                    ->wherePivot('torneo_id', $id);
    }
    public function torneosPorId2($id,$apodo)
    {
        return $this->belongsToMany('App\Torneo', 'torneo_usuario', 'usuario_id', 'torneo_id')
                    ->withPivot('torneo_id','vidas_restantes','apodo')
                    ->wherePivot('torneo_id', $id)
                    ->wherePivot('apodo',$apodo)
                    ->using('App\TorneoUsuario');
    }
    
    public function jornadas()
    {
        return $this->belongsToMany('App\Jornada', 'jornada_user', 'usuario_id', 'jornada_id');
    }
    
    public function jornadasApodo($apodo)
    {
        return $this->belongsToMany('App\Jornada', 'jornada_user', 'usuario_id', 'jornada_id')
                    ->withPivot('apodo', 'equipo_logo', 'equipo_seleccionado','resultado')
                    ->wherePivot('apodo', $apodo);
    }
    
    public function jornadaValidacion($apodo, $jornadaId)
    {
        return $this->belongsToMany('App\Jornada', 'jornada_user', 'usuario_id', 'jornada_id')
                    ->withPivot('apodo', 'jornada_id')
                    ->wherePivot('apodo', $apodo)
                    ->wherePivot('jornada_id', $jornadaId);
    }
    public function userRole($roleID)
    {
        return $this->belongsToMany('App\Role')->where('role_id',$roleID);
    }
}


