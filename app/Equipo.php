<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    //
    protected $table = 'equipos';
    protected $guarded = ['id'];
    
    public function jornadas()
    {
        return $this->belongsTo('App\Jornada');
    }
    public function encuentro()
    {
        return $this->hasOne('App\Encuentro','equipo_local_id');
    }
    public function encuentrodos()
    {
        return $this->hasOne('App\Encuentro','equipo_visitante_id');
    }
}
