<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jornada extends Model
{
    //
    protected $table = 'jornadas';
    protected $guarded = ['id'];
    
    public function encuentro()
    {
        return $this->belongsToMany('App\Encuentro');
    }
    
    public function usuarios()
    {
        return $this->belongsToMany('App\User', 'jornada_user', 'jornada_id', 'usuario_id')
                    ->withPivot('apodo','resultado','equipo_seleccionado')
                    ->using('App\JornadaUser');
    }
    
    public function encuentros()
    {
        return $this->hasMany('App\Encuentro');
    }
    
    public function encuentroEquipo($equipo,$id_jornada)
    {
        return $this->hasMany('App\Encuentro')->where('equipo_local_id',$equipo)->orWhere('equipo_visitante_id',$equipo)->where('jornada_id',$id_jornada);
    }
    
}
    