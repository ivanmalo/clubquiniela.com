<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TorneoUsuario extends Pivot
{
    //
    protected $table = 'torneo_usuario';
    protected $guarded = ['id'];
    
    public function usuario()
    {
        return $this->belongsTo('App\User','usuario_id');
    }
    
    public function torneo()
    {
        return $this->belongsTo('App\Torneo','torneo_id');
    }
    
    public function archivo()
    {
        return $this->belongsTo('App\Archivo','torneo_id');
    }
    
    public function getCreatedAtColumn()
    {
        return 'created_at';
    }
 
    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }
    
    
    
}
