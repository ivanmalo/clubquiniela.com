<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class JornadaUser extends Pivot
{
    //
    protected $table = 'jornada_user';
    protected $guarded = ['id'];
    
    public function getCreatedAtColumn()
    {
        return 'created_at';
    }
 
    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }
    
    
}
