<?php

//------
use App\Encuentro;
use App\EncuentroUsuario;
use App\Equipo;
use App\Jornada;
use App\Participante;
use App\Role;
use App\Permission;
use App\User;
use App\Archivo;

function currentUser()
{
    return auth()->user();
}

function currentUserid()
{
    return auth()->user()->id;
}

function archivo($id)
{
    
    if($id){
       $archivo = Archivo::where('torneo_id', $id)->first();
       if($archivo){
           return $archivo;
       }
    }
    return ""; 
}


function tipoUsuario()
{
    
    $user = auth()->user();
    
    $userRol = $user->userRole(1)->first();
    
    
    return $userRol;
}

function horaPartido($horaPartido)
{
    
    $arrayHora = explode(":", $horaPartido);
    $transcurridoMinutos = intval( $arrayHora[1] ) - 30;
    if($transcurridoMinutos < 0) {
        $arrayHora[0] = intval( $arrayHora[0] ) - 1;
        $arrayHora[1] = 60 + intval($transcurridoMinutos);
    }
                  
    if( count( $arrayHora[0] ) < 2 ) {
        $arrayHora[0] = "0"+$arrayHora[0];
    }
    $horaParticipacion = implode(":", $arrayHora);
    return $horaInicio = $horaParticipacion;
}