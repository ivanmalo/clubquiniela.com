<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncuentroUsuario extends Model
{
    //
    protected $table = 'encuentro_usuario';
    protected $guarded = ['id'];
}
