<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordatorioUser extends Mailable
{
    use Queueable, SerializesModels;


    public function __construct()
    {
    }

    public function build()
    {
        $address = 'contacto@clubquiniela.com';
        $subject = 'Te invitamos al nuevo torneo de apertura Liga MX Apertura 2018';
        $name = 'Club Quiniela';
        
        return $this->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->markdown('emails.recordatorio');
    }
}
