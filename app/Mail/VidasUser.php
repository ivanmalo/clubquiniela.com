<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VidasUser extends Mailable
{
    public $email;
    public $apodos;
    public $user;
    public $jornada;
    public $torneo;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$user,$apodos,$jornada,$torneo)
    {
        //
        $this->email = $email;
        $this->apodos = $apodos;
        $this->user = $user;
        $this->jornada = $jornada;
        $this->torneo = $torneo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.vidas')->subject('Reportde de Vidas');
    }
}
