$(".btn.red.success").click(function(event) {
    event.preventDefault();
    var req = $(this);
    var id = req.attr('id-btn');
    var mensaje = req.attr('mensaje');
    if(!mensaje){
        mensaje = req.attr('mensaje'+id);
        if(!mensaje){
            mensaje = 'Confirmar selección de equipo. ' + 
                      '\n\rPresione Ok para continuar.';
        }
    }
    //console.log(id);   
    bootbox.confirm({
                        message: mensaje,
                        className: "bootbox modal fade bootbox-alert in",
                        buttons: {
                            confirm: {
                                label: 'Ok',
                                className: 'waves-effect waves-light light-blue alert-btn'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'waves-effect waves-light red darken-3'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $('#btn-success'+id).submit();
                            }
                        }
                    });
});

$("#formtransferencia").click(function(event) {
    
    console.log("loquesa");
    event.preventDefault();
    var req = $(this);
    var id = req.attr('id-btn');
    var mensaje = req.attr('mensaje');
    if(!mensaje){
        mensaje = req.attr('mensaje'+id);
        if(!mensaje){
            mensaje = 'Confirmar que quieres comprar entrada.' + 
                      '\n\rPresione Ok para continuar.';
        }
    }
    //console.log(id);   
    bootbox.confirm({
                        message: mensaje,
                        className: "bootbox modal fade bootbox-alert in",
                        buttons: {
                            confirm: {
                                label: 'Ok',
                                className: 'waves-effect waves-light light-blue alert-btn'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'waves-effect waves-light red darken-3'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $("#formcomprar").submit();
                            }
                        }
                    });
});