@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                
                                <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">Recuperar Contraseña</h4>
                            </div>
                
                                <div class="panel-body">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                
                                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                        {{ csrf_field() }}
                
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>
                
                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Enviar link para recuperar contraseña
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                
                                </div>
                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection