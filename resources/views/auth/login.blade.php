@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <!--<a style="float:left;" href="#">¿Cómo funciona?</a>-->
                                    
                                    
                                    <div class="col-md-8">
                                     @foreach($reglas as $regla)
                                     
                                     <div class="col-md-6">  <img style="float:left;" src="@if($regla->nombre == 'Reglas NFL') {{asset('images/log_nfl.jpg')}} @else {{asset('images/ligamx.png')}} @endif" width="30" height="30"></img> <a href="{{route('reglas.show',['id'=>$regla->id])}}" style="float:left;    margin-left: 4px;" class="{{$regla->nombre}}">{{$regla->nombre . "&nbsp;"}}</a></div>
                                     
                                     @endforeach
                                     </div>
                                    <div class="col-md-4"><a style="float:right;" href="{{ route('register') }}">Crear Cuenta</a></div>
                                     
                                    <br>
                                    <br>
                                </div>
                                
                                {{-- 
                                    <div class="col-sm-12">    
                                        <div class="alert alert-success show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h5 class="alert-heading text-center">!Ya disponible¡ Torneo Liga MX Apertura 2019 $500.00 MXN</h5>
                                            <p>Te invitamos al nuevo torneo Liga MX Apertura 2019 ¿Crees poder sobrevivir?</p>
                                            <p>1 vida para 17 Jornadas</p>
                                            <p>Costo por participación <b>$ 500.00 MXN</b></p>
                                            <p class="text-center"> <b>!Compra tu entrada¡</b> </p>
                                             
                                            <p>Inicia sesión -> Comprar Torneo Revancha</p>
                                        </div>
                                    </div>
                                --}}
                                
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="index.html" class="text-success">
                                            <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                            
                                        </a>
                                    </h2>
                                    <h4 class="text-uppercase font-bold m-b-0">Iniciar Sesión</h4>
                                </div>
                                
                                <div class="col-sm-12">
                                    <br>
                                    <br>
                                </div>
                                
                                <div class="panel-body">
                                    
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>
                
                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                
                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control" name="password" required>
                
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        {{--
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Mantener sesión
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        --}}
                
                                        <div class="form-group account-btn text-center col-sm-12">
                                            <button type="submit" class="btn btn-primary">
                                                Entrar
                                            </button>
                                        </div>
                                        
                                        <div class="form-group account-btn text-center col-sm-12">
                                            
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                ¿Olvidaste tu contraseña?
                                            </a>
                                        </div> 
                                        
                                    </form>
                                    
                                </div>
                                
                                
                                
                            
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
