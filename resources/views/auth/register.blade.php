@extends('layouts.registro')
@section('pageTitle', 'Registrar')
@section('content')
<section>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrapper-page">
                <div class="m-t-40 card-box">
                    <div class="row">
                         <div class="col-md-8">
                                     @foreach($reglas as $regla)
                                     
                                     <div class="col-md-6">  <img style="float:left;" src="@if($regla->nombre == 'Reglas NFL') {{asset('images/log_nfl.jpg')}} @else {{asset('images/ligamx.png')}} @endif" width="30" height="30"></img> <a href="{{route('reglas.show',['id'=>$regla->id])}}" style="float:left;    margin-left: 4px;" class="{{$regla->nombre}}">{{$regla->nombre . "&nbsp;"}}</a></div>
                                     
                                     @endforeach
                                     </div>
                        
                        <div class="col-sm-4">
                           
                        
                            <a style="float:right;" href="{{ route('login') }}">Iniciar Sesión</a>
                        </div>
                    </div>
                    
                <div class="text-center">
                    <h2 class="text-uppercase m-t-0 m-b-30">
                        <a href="index.html" class="text-success">
                            <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                            
                        </a>
                    </h2>
                    <h4 class="text-uppercase font-bold m-b-0">Regístrate</h4>
                </div>
                
                <div class="col-sm-12">
                    <br>
                    <br>
                </div>

                <div class="account-content">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                                <label for="name" class="control-label">Nombre Completo</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('fecha_nacimiento') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                                <label for="fecha_nacimiento" class="control-label">Fecha de Nacimiento</label>
                                <input  type="date" name="fecha_nacimiento"  class="form-control datepicker2">
                                        
                                @if ($errors->has('fecha_nacimiento'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                                <label for="telefono" class="control-label">Teléfono</label>
                                <input id="telefono" type="number" name="telefono" class="form-control" value="{{ old('telefono') }}">
                                @if ($errors->has('telefono'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('como_se_entero') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                               <label for="como_se_entero" class="control-label">¿Cómo se entero?</label> 
                               <input id="como_se_entero" class="form-control" type="textarea" name="como_se_entero" value="{{ old('como_se_entero') }}"/>
                                @if ($errors->has('como_se_entero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('como_se_entero') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        {{-- 
                        <div class="form-group m-b-20">
                            <div class="col-xs-12">
                                <label for="cuenta" class="control-label">Numero de Cuenta</label>
                                <input id="cuenta" type="number" name="cuenta" class="form-control">
                            </div>
                        </div>
                        --}}
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                                <label for="email" class="control-label">Correo Electrónico</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} m-b-20">
                            <div class="col-xs-12">
                                <label for="password" class="control-label">Contraseña</label> 
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group m-b-20">
                            <div class="col-xs-12">
                                <label for="password-confirm" class="control-label">Confirmar Contraseña</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
              
                        <div class="form-group account-btn text-center m-t-10">
                            <div class="col-xs-12">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

@section('scripts')

@endsection