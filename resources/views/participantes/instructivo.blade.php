@extends('layouts.registro')
@section('pageTitle', 'Puntuaciones de Partidos')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
            <h3 class="panel-title panel-title-color">Instructivo</h3>
          </div>
          <div class="panel-body">
            <div class="panel-body">
                <div class="account-content">
                    
                    <div class="">
                        <img with="100" height="200" class="center-block" src="http://survival-mikipdia.c9users.io/storage/files/img/qqq.jpg"></img>
                    </div>
                        
                    <h1><strong>Instructivo</strong></h1>
                 
                    
                        <h4>Hoja menu</h4>
                    <p>
                        Este es el menú principal del archivo, en la parte superior tenemos el menú principal que será el mismo en todas las hojas del archivo, presionando sobre un nombre en dicho menú iremos a la hoja respectiva.En esta hoja seleccionamos nuestro País/Ciudad para que todos los horarios de los juegos estén en nuestro horario local 
                    </p>
                    <p>
                        También permite indicar si queremos o no usar el horario de verano (en caso de que aplique), podemos hacer clic derecho sobre el recuadro azul y se marcará automáticamente o podemos colocar cualquier letra en la casilla para marcarla. 
                    </p>
                   
                    
                            <h4>Hoja Historia</h4>
                    <p>
                        Esta hoja está dividida en tres partes (aparecen debajo del menú principal):
                        <ul>
                            <li>Historia: Nos muestra un pequeño resumen de lo que ha sido la historia de la Copa Mundial </li>
                            <li>Ganadores: Nos muestra cronológicamente todos los ganadores de los mundiales realizados hasta Brasil 2014</li>
                            <li>Goleadores: Nos muestra cronológicamente todos los líderes goleadores en cada una de las ediciones de la Copa Mundial. </li>
                            
                        </ul>
                    </p>
                    
                    <h1><strong>Para rellenar la quiniela seguir este paso</strong></h1>
                    <h4>Hoja calendario</h4>
                    <p>
                        Esta hoja está dividida en 14 partes (aparecen debajo del menú principal), esta hoja y sus divisiones son las hojas principales donde se cargan los resultados de los juegos. 
                    </p>
                    <p>
                        Grupo A, B, C, D, E, F, G, H: Esta es la fase de grupos, el resultado/pronostico se coloca en la columna Resultado y la tabla inferior se actualizará de forma automática; si al final de los juegos del grupo llegase a existir un empate entre 2 o más equipos, el desempate se regirá por las reglas indicas en el apartado Reglas; si al final fuese necesario, se dispondrá de una tabla auxiliar donde se podrá indicar de forma manual cual es la clasificación final de los equipos de dicho grupo. 
                    </p>
                    <p>
                       Octavos de Final: esta hoja nos muestra los emparejamientos de octavos de final donde debemos también llenar los resultados/pronósticos de cada jeugo, al ir llenando los resultados automáticamente se actualizan los nombres de los equipos en la siguiente fase  
                    </p>
                    <p>
                        Cuartos de Final, Semifinales y Gran Final: estas hojas muestran la respectiva fase donde debemos también llenar los resultados de nuestros pronósticos, al ir llenando los resultados automáticamente se actualizan los nombres de los equipos en la siguiente fase. 
                    </p>
                    <p>
                        Todo: Nos mostrará una tabla con todos los juegos y remarcara los nombres de los equipos que hayan ganado dicho juego, esto nos da una visión general de los resultados. 
                    </p>
                    <p>
                        Diario: Nos muestra los juegos del día seleccionado, los resultados también se pueden cargar desde esta pantalla, para seleccionar un día simplemente hacemos clic sobre cualquier parte del recuadro del día indicado. 
                    </p>
                    <h4>Hoja equipos</h4>
                    <p>
                        Esta hoja nos muestra un pequeño resumen de cada una de las selecciones participante en la copa, para cambiar de país simplemente presionamos sobre la bandera del mismo. 
                    </p>
                    <h4>Hoja reglas</h4>
                    <p>
                        Esta hoja nos muestra un pequeño resumen de las reglas que rigen el torneo, sobre todo lo referente al desempate cuando existan equipos empatados al finalizar la ronda de grupos.  
                    </p>
                    <h4>Hoja resumen</h4>
                    <p>
                      Esta hoja nos muestra un fixture del torneo para una mejor visualización del desarrollo del mismo.    
                    </p>
                    <h4>Hoja estadistica</h4>
                    <p>
                        Esta hoja nos muestra una tabla con todos los datos sobre Partidos Jugados, Ganados, Perdidos, Goles a favor, etc. De cada uno de los 32 equipos participantes, estos datos son en base a la información/pronósticos que hemos llenado en las hojas anteriores 
                    </p>
                    <p>
                     Al presionar sobre cualquiera de los encabezados de la tabla, esta se ordena por dicho campo en forma ascendente y se presiona nuevamente en forma descendente    
                    </p>
                    <h4>Hoja ver quiniela</h4>
                    <p>
                        Esta hoja es el resumen de nuestros pronósticos, esta hoja es la que se envía al administrador de la quiniela (si estamos participando en alguna quiniela); de esta hoja se puede hacer una copia en archivo Excel o copiar los datos en Texto, ver abajo
                    </p>
                    <p>
                        Nota: Los resultados NO se cargan en esta hoja, todos los resultados se cargan por la pantalla correspondiente a la etapa del torneo o en la hoja diario. 
                    </p>
                    <p>
                        Con el botón Completar Datos Personales, nos aparecerá un formulario donde podemos completar la información que sea necesaria para el administrador de la quiniela. 
                    </p>
                    <p>
                        Con el botón Crear Archivo de Excel, se creara una copia de esta hoja en el escritorio de nuestra computadora, este archivo debe ser enviado luego al administrador de la quiniela 
                    </p>
                    <p>
                        Con el botón Copiar Datos al Clipboard, se crearan los datos de los pronósticos en formato texto y se copiaran al portapapeles de Windows, de esta manera podremos pegarlos directamente en un correo para enviarlo al administrador de la quiniela 
                    </p>
                    
                    <h4>Hoja comentario</h4>
                    <p>Esta hoja es conseguirán información para las personas que deseen armar y administrar una quiniela. </p>
                    
                    
                    
                        
                    
                    
                    
                    
                    
                </div>
                
                                   
          </div>
        </div>
    </div>
</div>
@endsection