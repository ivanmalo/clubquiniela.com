@extends('layouts.principal2')
@section('pageTitle', 'Liga MX Clausura Revancha 2019 ')
@section('content')

    @include('mensajes/alertas')
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-personalizado">
                
                <div class="panel-heading">
                    <h3 class="panel-title panel-title-color"> 
                        <i class="mdi mdi-soccer icono-compra"></i> Liga MX Clausura Revancha 2019 $500.00 MXN
                    </h3>
                </div>
                
                <div class="panel-body">
                    <div class="panel-body">
                        <div class="account-content">
                            <div class="row">
                                <div class="col-12">
                                    @if(true)
                                        @if($torneoRevancha < 3)
                                            <div class="text-center">
                                                <h5 class="text-uppercase font-bold m-b-0">Liga MX Apertura Revancha $500.00 MXN</h5>
                                                <h5 class="text-uppercase font-bold m-b-0">Inicio del torneo 8 de febrero del 2019</h5>
                                                <h5 class="text-uppercase font-bold m-b-0">Reglas del juego <a href="{{route('reglas.show',['id'=>2])}}" target="blanck_">Aquí</a></h5>
                                                <h6 class="text-uppercase font-bold m-b-0">Aún puedes comprar {{ 3 - $torneoRevancha }} entradas.</h6>
                                                <br>
                                            </div>
                                             
                                            <div class="col-12 col-sm-12 col-md-6" role="group" aria-label="...">
                                                <div class="text-center">
                                                    <h5>Pago con paypal</h5>
                                                    <br>
                                                </div>
                                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                    <input type="hidden" name="cmd" value="_s-xclick">
                                                    <input type="hidden" name="hosted_button_id" value="Y5S9BD7RY9KEN">
                                                    
                                                    <div class="form-group">
                                                        <input type="hidden" name="on0" value="Tu correo">
                                                        <label class="control-label">Tu correo:</label>
                                                        <input class="form-control" type="text" readonly name="os0" maxlength="200" value="{{$correo}}">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <input type="hidden" name="on1" value="Tu nombre">
                                                        <label class="control-label">Tu nombre:</label>
                                                        <input class="form-control" type="text" value="{{$nombre}}" readonly name="os1" maxlength="200">  
                                                    </div>
                                                    
                                                    <div class="form-group" style="text-align: center;">
                                                        <input type="image" src="https://www.paypalobjects.com/en_US/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                    </div>
                                                </form>
                                                <br>
                                                <br>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 col-md-6 text-center">
                                                <form action="{{ route('particioantes.torneoRevanchaTransferencia') }}" method="post" id="formcomprar" >
                                                    {{ csrf_field() }}
                                                    <h5>Pago con transferencia</h5>
                                                    <p>Su participación se validará inmediatamente, sin embargo el pago se debe hacer lo antes posible o su registro será dado de baja automáticamente.</p>
                                                    <br>
                                                    <input type="submit" class="btn btn-primary" id="formtransferencia" value="Comprar torneo">
                                                </form>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 col-md-6 text-center">
                                                <br />
                                                <br />
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 text-center">
                                                <h5>Pago en Efectivo</h5>
                                                <p>Con los Administradores</p>
                                            </div>
                                        @else
                                            <div class="text-center">
                                                <h5 class="text-uppercase font-bold m-b-0">Has superado el número máximo de entradas. Gracias por tu preferencia.</h5>
                                                <br>
                                            </div>
                                        @endif
                                    @else
                                        <div class="text-center">
                                            <h5 class="text-uppercase font-bold m-b-0">Próximamente nuevos torneos.</h5>
                                            <br>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
@endsection