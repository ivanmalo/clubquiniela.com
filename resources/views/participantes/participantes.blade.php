@extends('layouts.principal2')
@section('pageTitle', 'Puntuaciones de Partidos')
@section('content')
     <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                          <div class="panel-heading">
                           
                            <h3 class="panel-title panel-title-color">{{currentUser()->name}} - Liga MX Clausura 2018</h3>
                          </div>
                          <div class="panel-body">
                              <div style="overflow-y: auto; height:500px; ">
                                <table  class="table table-fixed table-text2 table-striped table-bordered">
                                    
                                    @foreach($jornadas as $jornada)
                                    <thead>
                                        <tr>
                                            <th class="{{ ($jornada->id == $jornadaActiva->id)? 'active-jornada' : ''}}" colspan="4">{{$jornada->clave_jornada}}</th>
                                        </tr>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th colspan="2">Partido</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($jornada->encuentros as $encuentro)
                                        
                                                <tr>
                                                    <td>{{$encuentro->fecha_inicio}}</td>
                                                    <td>{{$encuentro->hora_inicio}}</td>
                                                    <td>{{$encuentro->equipoLocal->equipo}}<br><img src="{{ asset('images/equipos/'.$encuentro->equipoLocal->escudo) }}" style=" width: 10%;"></td>
                                                    <td>{{$encuentro->equipoVisitante->equipo}}<br ><img src="{{ asset('images/equipos/'.$encuentro->equipoVisitante->escudo) }}" style=" width: 10%;"></td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    
    
@endsection

@section('scripts')

@endsection