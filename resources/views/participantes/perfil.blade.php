@extends('layouts.principal2')
@section('pageTitle', 'Perfil')
@section('content')
<!--Seccion del perfil-->
                        <div class="container">
                          <div class="m-t-30">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#home-b1" data-toggle="tab" aria-expanded="true">
                                        Mi Perfil
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#profile-b1" data-toggle="tab" aria-expanded="false">
                                        Ajustes
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home-b1">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <!-- Personal-Information -->
                                            <div class="panel panel-personalizado panel-fill">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title panel-title-color">Información Personal</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="m-b-20">
                                                        <strong>Nombre Completo</strong>
                                                        <br>
                                                        <p class="text-muted">{{$usuario->name}}</p>
                                                    </div>
                                                    <div class="m-b-20">
                                                        <strong>Telefono</strong>
                                                        <br>
                                                        <p class="text-muted">{{$usuario->telefono}}</p>
                                                    </div>
                                                    <div class="m-b-20">
                                                        <strong>Email</strong>
                                                        <br>
                                                        <p class="text-muted">{{$usuario->email}}</p>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Personal-Information -->
                                        </div>


                                        <div class="col-md-8">
                                            <!-- Personal-Information -->
                                            <div class="panel panel-success panel-fill">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title panel-title-color">Participa en Torneo</h3>
                                                </div>
                                                <div class="panel-body">
                                                  @foreach($misTorneos as $torneo)
                                                  <h5 class=" text-uppercase">{{$torneo->nombre}}</h5>
                                                  <div class="table-responsive">
                                                        <table   class="table table-text table-striped table-bordered">
                                                            <thead>
                                                                <th>#</th>
                                                                <th>Vidas</th>
                                                                <th>Apodos</th>
                                                                <th>Jornada 1</th>
                                                                <th>Jornada 2</th>
                                                                <th>Jornada 3</th>
                                                                <th>Jornada 4</th>
                                                                <th>Jornada 5</th>
                                                                <th>Jornada 6</th>
                                                                <th>Jornada 7</th>
                                                                <th>Jornada 8</th>
                                                                <th>Jornada 9</th>
                                                                <th>Jornada 10</th>
                                                                <th>Jornada 11</th>
                                                                <th>Jornada 12</th>
                                                                <th>Jornada 13</th>
                                                                <th>Jornada 14</th>
                                                                <th>Jornada 15</th>
                                                                <th>Jornada 16</th>
                                                                <th>Jornada 17</th>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($torneo->misUsuarios($usuario->id)->get() as $numero => $user)
                                                                  <tr>
                                                                  <td>{{$numero + 1}}</td>
                                                                  <td>{{$user->pivot->vidas_restantes}}</td>
                                                                  <td>{{$user->pivot->apodo}}</td>
                                                                  @foreach($user->jornadasApodo($user->pivot->apodo)->get() as $jornada)
                                                                  
                                                                      <td class="{{$jornada->pivot->resultado}}">
                                                                            @if($jornada->participacion)
                                                                               @if($jornada->pivot->usuario_id == $usuarioId)
                                                                               
                                                                                   <img style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                               @else
                                                                                   <img style=" width: 50%;"  class="participantes_logo_equipo" src="{{asset('images/pick_escondido.png')}}"></img>
                                                                               @endif
                                                                            @else
                                                                                <img  style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                            @endif
                                                                       </td>
                                                                  @endforeach
                                                                  </tr>
                                                                @endforeach
                                                            </tbody>
                                                          
                                                        </table>
                                                    </div>
                                                  @endforeach
                                                </div>
                                            </div>
                                            <!-- Personal-Information -->

                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-b1">
                                    <!-- Personal-Information -->
                                    <div class="panel panel-personalizado panel-fill">
                                        <div class="panel-heading">
                                            <h3 class="panel-title panel-title-color">Editar Perfil</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form action="{{route('participantes.updatePass')}}"  class="form-validation">
                                            <div class="form-group">
                                                <label for="userName">Nombre<span class="text-danger">*</span></label>
                                                <input type="text" disabled="" value="{{$usuario->name}}" name="name" parsley-trigger="change" required
                                                       placeholder="Enter user name" class="form-control" id="userName">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailAddress">Email <span class="text-danger">*</span></label>
                                                <input type="email" disabled="" value="{{$usuario->email}}" name="email" parsley-trigger="change" required
                                                       placeholder="Enter email" class="form-control" id="emailAddress">
                                            </div>
                                            <div class="form-group">
                                                <label for="pass1">Contraseña <span class="text-danger">*</span></label>
                                                <input id="pass1" type="password" name="password" placeholder="Contraseña" required
                                                       class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="passWord2">Confirmar Contraseña <span class="text-danger">*</span></label>
                                                <input data-parsley-equalto="#pass1" type="password" required
                                                       placeholder="Contraseña" class="form-control" id="passWord2">
                                            </div>

                                            <div class="form-group text-right m-b-0">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                    Actualizar
                                                </button>
                                            </div>

                                        </form>

                                        </div>
                                    </div>
                                    <!-- Personal-Information -->
                                </div>
                                
                            </div>
                        </div>
                      </div>
<!--EndSeccion del Perfil-->




@endsection