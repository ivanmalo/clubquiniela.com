@extends('layouts.principal2')
@section('pageTitle', 'Puntuaciones de Partidos')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
            <h3 class="panel-title panel-title-color">Reglas</h3>
          </div>
          <div class="panel-body">
            <div class="panel-body">
                                <div class="account-content">
                                    <p>
                                        <b>IMPORTANTE: Nos reservamos el derecho de Admisión</b>
                                        <br>
                                        <br>
                                        En caso de que existan controversias no previstas en las reglas, será decisión irrevocable del comité organizador tomar la mejor decisión para el bienestar de todos los jugadores
                                        <br>
                                        <br>
                                        <b>Mecánica</b>
                                        <br>
                                        A través de nuestra página de Internet www.clubquiniela.com cada participante deberá escoger un equipo en cada jornada. En caso de que dicho equipo resulte vencedor o bien empate, el participante seguirá con vida en el certamen; por el contrario si dicho equipo resulta perdedor, el participante perderá una vida. El torneo revancha cuentan con una vida, los torneos de 250 y 500 cuentan con dos vidas.
                                        <br>
                                        Los Participantes Sobrevivientes deberán seleccionar un nuevo equipo, diferente a los previamente escogidos, e ingresarlo dentro de la página. Es decir que no podrá votar por equipos previamente seleccionados. 
                                        El día límite para ingresar a sus equipos en la página de Internet es de 30 minutos antes del primer partido de la jornada. En la página de Internet existe un Reloj, el cual se considera el "Reloj Oficial". 
                                        <br>
                                        <br>
                                        <b>IMPORTANTE: EN CASO DE NO REALIZAR SU SELECCION EN TIEMPO, EL PARTICIPANTE PERDERA UNA VIDA AUTOMATICAMENTE SIN EXCEPCIÓN ALGUNA. </b>
                                        <br>
                                        <br>
                                        <b>ACLARACIÓN:</b> Por seguridad de la información los participantes, los votos no se pueden cambiar bajo ninguna circunstancia. Les recordamos analizar bien sus votos ya que bajo ninguna excepción realizaremos cambios.
                                        <br>
                                        <br>
                                        Como ayuda a nuestros Participantes, el sistema manda 2 recordatorios vía correo electrónico, 24  horas antes de la jornada, a los Participantes que no han seleccionado equipo.
                                        El Ganador o Ganadores serán aquellos Participantes que lleguen más lejos en el Certamen (Únicamente Temporada Regular).
                                        El número máximo de Participaciones por TORNEO por persona es de 3. 
                                        <br>
                                        <br>
                                        El Premio a repartir será la Bolsa Acumulada de todas las Participaciones de Cada TORNEO, restando a esto el 10% de Gastos Administrativos.
                                        IMPORTANTE: El sobreprecio por el uso de la PASARELA DE PAGO, no formarán parte de la  Bolsa a Repartir.
                                        En caso de empate, dicho premio será dividido, en partes iguales, entre el número de Ganadores.
                                        Una vez cerradas las inscripciones, el Comité Organizador dará a conocer el Monto de cada Bolsa.
                                        <br>
                                        <br>
                                        Agradeceremos efectuar el pago de las quinielas a través de LA PASARELA DE PAGO encontrada en la pagina.
                                        <br>
                                        <br>
                                        <b>NOTAS IMPORTANTES:</b>
                                        <br>
                                        -NO PUEDES ELEGIR EQUIPOS DE UN PARTIDO PROGAMADO PARA OTRA FECHA QUE NO SEA LA JORNADA ORIGINAL.
                                        <br>
                                        -SI EL PARTDO ES CANCELADO ANTES DEL SILBATAZO INICIAL, AMBOS EQUIPOS SIGUEN CON VIDA.
                                        <br>
                                        -SI EL PARTIDO ES CANCELADO DESPUES DEL SILBATAZO INICIAL, SE CONSIDERARA EL RESULTADO PARCIAL COMO MARCADOR FINAL. ELIMINANDO AL EQUIPO QUE VAYA PERDIENDO.
                                        <br>
                                        -SEÑALAMOS QUE CADA TORNEO ES BOLSA INDEPENDIENTE
                                        <br>
                                        <br>
                                        
                                        <b>TORNEO Quiniela, Fechas y Cuotas</b>
                                        <br>
                                        
                                        <b>TORNEO REVANCHA 2018</b>
                                        <br>
                                        1.	Este TORNEO inicia en la Jornada 7. El primer juego de esta Semana es el día 13 DE FEBRERO DEL 2018 por lo que el límite de selección de equipos es el día MARTES 13 DE FEBRERO DEL 2018, 30 minutos antes del primer encuentro.
                                        <br>
                                        2.	Cuenta con un cupo máximo de 500 participaciones.
                                        <br>
                                        3.	Cada usuario puede adquirir hasta 3 participaciones en este TORNEO.
                                        <br>
                                        4.	El costo de cada participación es de $ 250.00 MXN.
                                        <br>
                                        5.	La fecha límite para cualquier inscripción a este TORNEO, será el dia LUNES 12 DE FEBRERO DEL 2018 las 18:00 Horas ( 6:00 PM ). A partir de dicho momento, no será posible recibir más inscripciones
                                        <br>
                                        6.	Cuenta con solo una vida por participación.
                                        <br>
                                        <br>
                                        
                                        
                                        <b>TORNEO de 250</b>
                                        <br>
                                        1.	Este TORNEO inicia en la Jornada 1 de la Liga MX. El primer juego es el día 5 DE ENERO DEL 2018, por lo que el límite de selección de equipos es 30 minutos antes de iniciar el partido.
                                        <br>
                                        2.	Cuenta con un cupo máximo de 500 participaciones.
                                        <br>
                                        3.	Cada usuario puede adquirir hasta 3 participaciones en este TORNEO.
                                        <br>
                                        4.	El costo de cada participación es de $ 250.00 MXN.
                                        <br>
                                        5.	La fecha límite para cualquier inscripción a este TORNEO, será el día 5 de ENERO del 2018, a las 18:00 Horas ( 6:00 PM ). A partir de dicho momento, no es posible recibir más inscripciones
                                        <br>
                                        <br>
                                        <b>TORNEO de 500</b>
                                        <br>
                                        1.	Este TORNEO inicia en la Jornada 1 de la Liga MX. El primer juego es el día 5 DE ENERO DEL 2018, por lo que el límite de selección de equipos es 30 minutos antes de iniciar el partido.
                                        <br>
                                        2.	Cuenta con un cupo máximo de 500 participaciones.
                                        <br>
                                        3.	Cada usuario puede adquirir hasta 3 participaciones en este TORNEO
                                        <br>
                                        4.	El costo de cada participación es de $ 500.00 MXN.
                                        <br>
                                        5.	La fecha límite para cualquier inscripción a este TORNEO, será el día 5 de ENERO del 2018, a las 18:00 Horas ( 6:00 PM ). A partir de dicho momento, no es posible recibir más inscripciones
                                        <br>
                                        <br>
                                    
                                    </p>
                                </div>
                            </div>
          </div>
        </div>
    </div>
</div>
@endsection