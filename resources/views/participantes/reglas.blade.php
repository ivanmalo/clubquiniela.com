@extends('layouts.principal2')
@section('pageTitle', 'Puntuaciones de Partidos')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
            <div class="panel-heading">
                <h3 class="panel-title panel-title-color">Reglas</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="account-content">
                         <?php echo $regla->contenido; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection