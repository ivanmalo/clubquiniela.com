    @if($message = Session::get('success'))
        <div class="alert alert-success fade in" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>{{ $message }}</p>
        </div>
    @endif
     
    @if ($message = Session::get('warning'))
        <div class="alert alert-warning fade in" id="error-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <p><b>¡Atención!</b> {{ $message }}</p>
            <br>
            <b>¡Importante! Al hacer la transferencia el concepto debe ser: "CQ Nombre Completo del Usuario"</b>
            <br>
            <b>Ejemplo: CQ Fernado Alvarez Alvarez</b>
            <br>
            <b>Mandar correo con su comprobante de pago a facturacion@clubquiniela.com y con mensaje.</b>
            <br>
            <b>Ejemplo: Nombre: Pepe Perez, Participación: 1. Adjuntar foto o archivo del comprobante.</b>
            <br>
            <br>
            <b>Manda tu pago a:</b>
            <br>
            <b>Julian Goldstone Urquiza</b>
            <br>
            <b>Bancomer</b>
            <br>
            <b>Cuenta 2978786224</b>
            <br>
            <b>Clabe 012680029787862248</b>
        </div>
    @endif 
     
    @if ($message = Session::get('error'))
        <div class="alert alert-danger fade in" id="error-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <p><b>¡Atención!</b> {{ $message }}</p>
        </div>
    @endif
    
    @if (count($errors) > 0)
        <div class="alert alert-danger fade in" id="error-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>   
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
