@extends('layouts.admin')
@section('pageTitle', 'Equipos')
@section('content')

        
            <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                          <div class="panel-heading">
                           
                            <h3 class="panel-title panel-title-color">Partidos</h3>
                          </div>
                          <div class="panel-body">
                               <div class="row">
                                    <div class="col-sm-4 col-sm-push-8">
                                        <a class="btn btn-primary" href="{{route('equipos.create')}}">Nuevo Equipo</a>
                                        
                                            <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                                     
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(count($equipos)>0)
                                        <table class="table table-text2 table-striped table-bordered">
                                            <thead>
                                                <th>Equipo</th>
                                                <th>Escudo</th>
                                                <th>Acción</th>
                                            </thead>
                                            <tbody>
                                                @foreach($equipos as $equipo)
                                                <tr>
                                                    <td>{{$equipo->equipo}}</td>
                                                    <td><img src="{{ asset('images/equipos/'.$equipo->escudo) }}" style=" width: 17%;"></img></td>
                                                    <td>
                                                        <a href="{{route('equipos.destroyEquipo',[$equipo->id])}}" class="btn btn-danger">Eliminar</a>
                                                        <a href="{{route('equipos.edit',[$equipo->id])}}" class="btn btn-primary">Editar</a>
                                                    </td>
                                                    
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                        @else
                                        <h3>No hay equipos registros</h3>
                                        @endif
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                
                <br />
                
            </div>
        </div>
    </div>
@endsection