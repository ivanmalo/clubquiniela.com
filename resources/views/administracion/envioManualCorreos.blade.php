@extends('layouts.admin')

@section('pageTitle', 'Administración')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-personalizado">
              <div class="panel-heading">
                <h1 class="panel-title panel-title-color">Administración</h1>
              </div>
            </div>
        </div>
    </div>

    @include('mensajes/alertas')
    
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#mail1">Mail Recordatorio</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#mail2">Mail Vidas Perdidas</a>
                    </li>
                </ul>
                
                <div class="tab-content">
                    <div id="mail1" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-sm12 col-md-5">
                                <h4>Mail Recordatorio</h4>
                                <br>
                                <br>
                            </div>
                            <div class="col-md-12">
                                <form action="{{ route('administracion.sendMailRecordatorio') }}" method="POST">
                                    {{ csrf_field() }}
                                    
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Enviar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <div id="mail2" class="tab-pane fade">
                        <div class="row">
                            <div class="col-sm12 col-md-5">
                                <h4>Mail vidas perdidas</h4>
                                <br>
                                <br>
                            </div>
                            <div class="col-md-12">
                                <form action="{{ route('administracion.sendMail') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12">
                                             <div class="form-group">
                                                <label for="correo">Correo para:</span></label>
                                                <select name="usuario_id" required class="form-control">
                                                    <option value="" selected>Seleccionar</option>
                                                    @foreach($usuarios as $usuario)
                                                        <option value="{{$usuario->id}}">{{$usuario->id." -- ".$usuario->name}}</option>
                                                    @endforeach
                                                </select> 
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="nombre">Apodos que perdieron vida:</span></label>
                                                <input type="text" name="apodos" required class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Enviar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>
@endsection