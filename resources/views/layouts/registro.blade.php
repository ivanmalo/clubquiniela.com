<!DOCTYPE html>
<html lang="es_MX">
    <head>
        <meta charset="utf-8" />
        <title>@yield('pageTitle')|Club Quiniela</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
       
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="icon" href="{{ URL::asset('images/favicon.ico') }}">

        <!--Morris Chart CSS -->
		<link href="{{ asset('plugins/morris/morris.css') }}" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="{{ asset('css/metisMenu.min.css') }}" rel="stylesheet">
        <!-- Icons CSS -->
        <link href="{{ asset('css/icons.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        
        
        <!-- Plugins css-->
        <link href="{{ asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/switchery/switchery.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet">
        
        
        
        <!--datatimepicker-->
        <link href="{{ asset('plugins/pickdate/themes/default.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/pickdate/themes/default.date.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/pickdate/themes/default.time.css') }}" rel="stylesheet">
        
        
        <title>Página Principal | Survival</title>
        
        <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
    </head>


    <body>
        
        @yield('content')

        <!-- js placed at the end of the document so the pages load faster -->
        
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>

        <script src="{{ asset('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}" type="text/javascript"></script>
        
        <script src="{{ asset('plugins/moment/moment.js') }}"></script>
        <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
        
        <!--Morris Chart-->
		<script src="{{ asset('plugins/morris/morris.min.js') }}"></script>
		<script src="{{ asset('js/raphael-min.js') }}"></script>

        <!-- Dashboard init -->
		<script src="{{ asset('js/jquery.dashboard.js') }}"></script>

        <!-- App Js -->
        <script src="{{ asset('js/jquery.app.js') }}"></script>
        
        <!-- form advanced init js -->
        <script src="{{ asset('pages/jquery.form-advanced.init.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.form-validation').parsley();
                $('.summernote').summernote({
                    height: 350,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
            });
        </script>
        
        
        <!--Pickdate-->
        {{--<script src="{{ asset('plugins/pickdate/jquery-1.11.1.min.js') }}"></script>--}}
        <script src="{{ asset('plugins/pickdate/legacy.js') }}"></script>
        <script src="{{ asset('plugins/pickdate/picker.js') }}"></script>
        <script src="{{ asset('plugins/pickdate/picker.date.js') }}"></script>
        {{-- <script src="{{ asset('plugins/pickdate/picker.time.js') }}"></script> --}}
        
        <script type="text/javascript">
            $(document).ready(function() {
               $('.datepicker2').pickadate({
                 // The title label to use for the month nav buttons
                   labelMonthNext: 'Mes siguiente',
                   labelMonthPrev: 'Mes anterior',
        
                 // The title label to use for the dropdown selectors
                   labelMonthSelect: 'Selecciona un mes',
                   labelYearSelect: 'Selecciona un año',
        
                 // Months and weekdays
                   monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
                   monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
                   weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
                   weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
        
                 // Materialize modified
                   weekdaysLetter: [ 'D', 'L', 'M', 'X', 'J', 'V', 'S' ],
                   
                   selectMonths: 12,
                   selectYears: 100,
                   min: new Date(1910,3,20),
                   max: new Date(2008,12,31),
                   format: 'dd/mmm/yyyy',
                   
                   formatSubmit: 'yyyy-mm-dd',
        
                 // Today and clear
                   today: 'Hoy',
                   clear: 'Limpiar',
                   close: 'Aceptar',
               });
           });
        </script>
        
        
        
        @yield('scripts')

    </body>
</html>