@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <a href="{{ route('home') }}">
                                        Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="{{ route('reglas') }}">
                                        Reglas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                                
                            <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">Torneo</h4>
                            </div>
                            
                            <div class="col-sm-12">
                                <br>
                                <br>
                            </div>
                            
                            <div class="panel-body">
                                <div class="account-content">
                                    
                                    <div style="width:100%; text-align: center;"  class="countdown center-block"></div>
                                        
                                        <div class="col-12">
                                            <div class="text-center">
                                                <h5 class="text-uppercase font-bold m-b-0">Torneos</h5>
                                                <br>
                                            </div>
                                            
                                            <div class="col-12">
                                                
                                                    
                                                    @foreach($torneos as $torneo)
                                                        <h6 class="text-uppercase font-bold m-b-0">{{ $torneo->nombre }}</h6>
                                                        <div class="table-responsive">
                                                            <table   class="table table-text table-striped table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Participantes</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($torneo->usuarios as $participante)
                                                                    <tr>
                                                                        <td>{{ $participante->pivot->apodo }}</td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    @endforeach
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
