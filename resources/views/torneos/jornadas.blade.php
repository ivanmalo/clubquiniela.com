@extends('layouts.admin')
@section('pageTitle', 'Jornadas')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-personalizado">
              <div class="panel-heading">
               
                <h1 class="panel-title panel-title-color">Jornadas</h1>
              </div>
            </div>
        </div>
    </div>
    
    @include('mensajes/alertas')
    
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-sm-10 col-sm-push-4">
                        <a href="{{route('emails.mailVidas',[$torneo_id])}}" class="btn btn-warning ">Email-Estado</a>
                        <a href="#" class="btn btn-success ">Email-Recordatorio</a>
                        <a href="{{route('torneo.createJornada',[$torneo_id])}}" class="btn btn-primary">Agregar Jornada</a>
                        <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                        
                        
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        @if(count($jornadas)>0)
                         <table class="table table-text table-striped table-bordered">
                             <thead>
                                 <th>#</th>
                                 <th>Estado</th>
                                 <th>Participación</th>
                                 <th>Acción</th>
                             </thead>
                             
                             @foreach($jornadas as $jornada)
                             <tr>
                                 <td>{{$jornada->clave_jornada}}</td>
                                 <td class="{{ ($jornada->estado == 'en_juego')? 'ganar' : '' }}">{{ ($jornada->estado == 'en_juego')? 'Jugando' : 'Pendiente' }}</td>
                                 <td>
                                    <div class="checkbox checkbox-custom">
                                        <form action="{{ route('torneo.updateJornada',[$torneo_id]) }}" id="Jparticipacion">
                                        @if($jornada->participacion)
                                            {!! Form::hidden('jornada_id',$jornada->id)!!}
                                            {!! Form::checkbox('participacion','1',true,['class'=>'validate checkbox', 'disabled', 'id'=>'checkbox']) !!}
                                            {!! Form::label('checkbox','Activa') !!}
                                        @else
                                            {!! Form::checkbox('participacion','0',false,['class'=>'validate','disabled']) !!}
                                            {!! Form::label('Desactivada') !!}
                                        @endif
                                            
                                        </form>
                                    </div>
                                    
                                 </td>
                                 <td>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-3">
                                        
                                        <form action="{{route('partidos.editJornada',[$torneo_id])}}">
                                            <input type="hidden" name="jornada_id" value="{{$jornada->id}}" />
                                            <input class="btn btn-primary" type="submit" value="Editar"/>
                                        </form>
                                    </div>
                                    <div class="col-lg-3">
                                        
                                        <form action="{{route('partidos.jornadaPartidos',[$torneo_id])}}">
                                            <input type="hidden" name="jornada_id" value="{{$jornada->id}}" />
                                            <input class="btn btn-custom" type="submit" value="Partidos"/>
                                        </form>
                                    </div>
                                    <div class="col-lg-2">
                                        <form action="{{route('torneo.destroyJornada',[$torneo_id])}}">
                                            <input type="hidden" name="id_jornada" value="{{$jornada->id}}" />
                                            <input class="btn btn-danger" type="submit" value="Eliminar"/>
                                        </form>
                                    </div>
                                    
                                </td>
                             </tr>
                             @endforeach
                            
                             
                         </table>
                         @else
                         <div>
                             <h3>No hay jornadas registras</h3>
                         </div>
                         @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')


@endsection