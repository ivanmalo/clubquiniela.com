@extends('layouts.admin')
@section('pageTitle', 'Torneos')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
           
            <h1 class="panel-title panel-title-color">Torneos</h1>
          </div>
        </div>
    </div>
</div>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-sm-2 col-sm-push-10">
                        <a class="btn btn-primary" href="{{route('torneo.crearTorneo')}}">Agregar Torneo</a>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                         <table class="table table-text table-striped table-bordered">
                             <thead>
                                 <th>Nombre Torneo</th>
                                 <th>Fecha Inicio</th>
                                 <th>Fecha Termino</th>
                                 <th>Acción</th>
                             </thead>
                             <tbody>
                                 @foreach($torneos as $torneo)
                                     <tr>
                                         <td>{{$torneo->nombre}}</td>
                                         <td>{{$torneo->fecha_inicial}}</td>
                                         <td>{{$torneo->fecha_final}}</td>
                                         <td>
                                            
                                             <a href="{{ route('torneo.jornadas',[$torneo->id]) }}" class="btn btn-warning">Calendario</a>
                                             
                                         </td>
                                     </tr>
                                 @endforeach
                             </tbody>
                             
                         </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection