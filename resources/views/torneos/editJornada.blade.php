@extends('layouts.admin')
@section('pageTitle', 'Modificar Jornada')
@section('content')

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
            <h3 class="panel-title panel-title-color">Crear Jornada</h3>
          </div>
          <div class="panel-body">
              <div class="row">
                    <div class="col-sm-4 col-sm-push-10">
                        <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                    </div>
                </div>
              <div class="row">
                    <div class="col-md-12">
                       <form action="{{ route('torneo.updateJornada',[$torneo_id]) }}" id="Jparticipacion">
                                 <div class="row">
                                     
                                     <div class="col-md-4">
                                     <div class="form-group">
                                        {{Form::label('clave_jornada', 'Clave', array('class' => 'awesome'))}}
                                        {{ Form::text('clave_jornada',$jornada->clave_jornada,['class' => 'form-control validate','required'])}}
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                     <div class="form-group">
                                        {{Form::label('estado', 'Estado de la Jornada', array('class' => 'awesome'))}}
                                        <select name="estado" class="sel1 form-control">
                                            @if($jornada->estado != "")
                                                <option value="no_activa" >No activa</option>
                                                <option value='en_juego' selected='selected'>Jugando</option>
                                            @else
                                                <option value="no_activa" selected="selected">No activa</option>
                                                <option value="en_juego">Jugando</option>
                                            @endif
                                        </select>
                                    </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <br>
                                            <div class="checkbox checkbox-custom">
                                                @if($jornada->participacion)
                                                    {!! Form::hidden('participacion','0') !!}
                                                    {!! Form::checkbox('participacion','1',true,['class'=>'validate checkbox', 'id'=>'checkbox']) !!}
                                                    {!! Form::label('checkbox','Permitir elegir equipo') !!}
                                                @else
                                                    {!! Form::hidden('participacion','0') !!}
                                                    {!! Form::checkbox('participacion','1',false,['class'=>'validate checkbox', 'id'=>'checkbox']) !!}
                                                    {!! Form::label('checkbox','Ocultar resultados') !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                               
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-push-10">
                                        <input type="hidden" name="jornada_id" value="{{$jornada->id}}"/>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Actualizar
                                        </button>
                                    </div>
                                </div>
                        
                        </form>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
                
            </div>
        </div>
    </div>
@endsection