@extends('layouts.admin')
@section('pageTitle', 'Crear Torneo')
@section('content')

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
            <h3 class="panel-title panel-title-color">Crear Jornada</h3>
          </div>
          <div class="panel-body">
              <div class="row">
                    <div class="col-sm-4 col-sm-push-10">
                        <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('torneo.storeJornada',[$torneo_id]) }}">
                                 <div class="row">
                                     <div class="col-md-12">
                                     <div class="form-group">
                                        <label for="clave_jornada">Clave</span></label>
                                        <input type="text" name="clave_jornada" parsley-trigger="change" required class="form-control" id="userName">
                                    </div>
                                    </div>
                                 </div>
                               
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-push-10">
                                        <input type="hidden" name="torneo_id" value="{{$torneo_id}}"/>
                                        <input type="hidden" name="participacion" value="0"/>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Registrar
                                        </button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
                
            </div>
        </div>
    </div>
@endsection