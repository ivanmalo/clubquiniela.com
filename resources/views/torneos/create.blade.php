@extends('layouts.admin')
@section('pageTitle', 'Crear Torneo')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
           
            <h1 class="panel-title panel-title-color">Crear Torneo</h1>
          </div>
        </div>
    </div>
</div>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                         <form action="{{ route('torneo.storeTorneo') }}">
                                 <div class="row">
                                     <div class="col-md-12">
                                     <div class="form-group">
                                        <label for="nombre">Nombre del Torneo</span></label>
                                        <input type="text" name="nombre" parsley-trigger="change" required class="form-control" id="userName">
                                    </div>
                                    </div>
                                 </div>
                               <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Fecha de Inicio</label>
                                         <div>
                                            <div class="input-group">
                                                <input name="fecha_inicial" type="text" class="form-control" placeholder="yyyy-mm-dd" id="datepicker-autoclose">
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Fecha de Termino</label>
                                         <div>
                                            <div class="input-group">
                                                <input name="fecha_final" type="text" class="form-control" placeholder="yyyy-mm-dd" id="datepicker-autoclose2">
                                            <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                              </div>
                                <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Numero Maximo de Participantes</label>
                                        <input name="no_max_usuarios" type="Number" class="form-control bfh-number" min="1" max="1000" maxlength="4">
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Numero Minimo de Participantes</label>
                                        <input name="no_min_usuarios" type="Number" class="form-control" min="1" max="1000" maxlength="4"> 
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Numero de Vidas</label>
                                        <input name="numero_vidas" type="Number" class="form-control bfh-number" min="1" max="2" maxlength="1">
                                    </div>
                                    </div>
                              </div>
                              <div class="form-group row">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                        Registrar
                                                    </button>
                                                </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection