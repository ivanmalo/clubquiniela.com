@extends('layouts.principal2')

@section('pageTitle', 'Puntuaciones de Partidos')

@section('content')
    @include('mensajes/alertas')
    
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                
                <div class="row">
                    
                    <ul class="nav nav-tabs">
                        @if(is_null(tipoUsuario()))
                            @foreach($misTorneos as $clave => $torneo)
                                <li class="{{ ($clave < 1)? 'active' : ''}}"><a data-toggle="tab" href="#{{$torneo->id}}">{{$torneo->nombre}}</a></li>
                            @endforeach
                        @else
                            @foreach($adminTorneo as $clave => $torneo)
                                <li class="{{ ($clave < 1)? 'active' : ''}}"><a data-toggle="tab" href="#{{$torneo->id}}">{{$torneo->nombre}}</a></li>
                            @endforeach
                        @endif
                        
                    </ul>
                @if(is_null(tipoUsuario()))
                    <div class="tab-content">
                        @foreach($misTorneos as $clave => $torneo)
                                <div id="{{$torneo->id}}" class="tab-pane fade {{ ($clave < 1)? 'in active': ''}}">
                                    
                                    @if( $torneo->jornadaActiva->first() )
                                        @if($torneo->id == 5)
                                            <div class="col-sm12 col-md-5">
                                                <h4>Tiempo restante para participar</h4>
                                                <input type="hidden" id="fecha_inicio" name="fecha_inicio" value="{{$fechaInicio}}"/>
                                                <input type="hidden" id="hora_inicio" name="hora_inicio" value="{{$horaInicio}} UTC"/>
                                                <div style="width:100%; text-align: center;" id="countdown-1" class="countdown center-block" ></div>
                                            </div>
                                        @elseif($torneo->id == 6)
                                            <div class="col-sm12 col-md-5">
                                                <h4>Tiempo restante para participar</h4>
                                                <input type="hidden" id="fecha_inicio_2" name="fecha_inicio_2" value="{{$fechaInicio2}}"/>
                                                <input type="hidden" id="hora_inicio_2" name="hora_inicio_2" value="{{$horaInicio2}} UTC"/>
                                                <div style="width:100%; text-align: center;" id="countdown-2" class="countdown center-block" ></div>
                                            </div>
                                        @endif
                                    @endif
                                    
                                    <div class="col-sm12 col-md-7">
                                        @if( count($sinApuesta) && $jornadaActiva[$torneo->id] )
                                        <h4>Seleccionar equipo:</h4>
                                        <div class="panel panel-personalizado">
                                            <div class="panel-body">
                                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                                    <div class="btn-group" role="group">
                                                        <form action="{{ route('juegos.createEquipos') }}" method="post" >
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-md-3">    
                                                                        <input type="hidden" name="torneo_id" value="{{ $torneo->id }}" />
                                                                        <label class="control-label">Usuario:</label>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-9">
                                                                        <select class="form-control" name="apodo">
                                                                        <option value="">Seleccionar</option>
                                                                        @foreach( $torneo->misUsuarios($usuarioId)->get() as $usuario)
                                                                            @if( array_search( $usuario->pivot->apodo, $sinApuesta ) !== false && $usuario->pivot->vidas_restantes)
                                                                    	        <option value="{{ $usuario->id.'-'.$usuario->pivot->apodo }}">{{ $usuario->pivot->apodo }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="submit" class="btn btn-primary" value="Escoger Equipo">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-text table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Vidas</th>
                                                        <th>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Usuario
                                                        </th>
                                                        @foreach($torneo->jornadaTorneo as $jornada1)
                                                            <th>{{ $jornada1->clave_jornada }}</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($torneo->usuarios as $numero => $participante)
                                                    <tr>
                                                        <td>{{$numero + 1}}</td>
                                                        <td class="vidas{{$participante->pivot->vidas_restantes}}">{{$participante->pivot->vidas_restantes}}</td>
                                                        <td>{{$participante->pivot->apodo}}</td>
                                                        
                                                        @foreach($participante->jornadasApodo($participante->pivot->apodo)->get() as $jornada)
                                                            <td class="{{$jornada->pivot->resultado}}">
                                                                @if($jornada->participacion)
                                                                    @if($jornada->pivot->usuario_id == $usuarioId)
                                                                        <img style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                    @else
                                                                        <img style=" width: 50%;"  class="participantes_logo_equipo" src="{{asset('images/pick_escondido.png')}}"></img>
                                                                    @endif
                                                                @else
                                                                    <img  style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                @endif
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                @else
                    <div class="tab-content">
                        @foreach($adminTorneo as $clave => $torneo)
                                <div id="{{$torneo->id}}" class="tab-pane fade {{ ($clave < 1)? 'in active': ''}}">
                                    
                                    @if( $torneo->jornadaActiva->first() )
                                        @if($torneo->id == 5)
                                            <div class="col-sm12 col-md-5">
                                                <h4>Tiempo restante para participar</h4>
                                                <input type="hidden" id="fecha_inicio" name="fecha_inicio" value="{{$fechaInicio}}"/>
                                                <input type="hidden" id="hora_inicio" name="hora_inicio" value="{{$horaInicio}} UTC"/>
                                                <div style="width:100%; text-align: center;" id="countdown-1" class="countdown center-block" ></div>
                                            </div>
                                        @elseif($torneo->id == 6)
                                            <div class="col-sm12 col-md-5">
                                                <h4>Tiempo restante para participar</h4>
                                                <input type="hidden" id="fecha_inicio_2" name="fecha_inicio_2" value="{{$fechaInicio2}}"/>
                                                <input type="hidden" id="hora_inicio_2" name="hora_inicio_2" value="{{$horaInicio2}} UTC"/>
                                                <div style="width:100%; text-align: center;" id="countdown-2" class="countdown center-block" ></div>
                                            </div>
                                        @endif
                                    @endif
                                    
                                    <div class="col-sm12 col-md-7">
                                        @if( count($sinApuesta) && $jornadaActiva[$torneo->id] )
                                        <h4>Seleccionar equipo:</h4>
                                        <div class="panel panel-personalizado">
                                            <div class="panel-body">
                                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                                    <div class="btn-group" role="group">
                                                        <form action="{{ route('juegos.createEquipos') }}" method="post" >
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-md-3">    
                                                                        <input type="hidden" name="torneo_id" value="{{ $torneo->id }}" />
                                                                        <label class="control-label">Usuario:</label>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-9">
                                                                        <select class="form-control" name="apodo">
                                                                        <option value="">Seleccionar</option>
                                                                        @foreach( $torneo->misUsuarios($usuarioId)->get() as $usuario)
                                                                            @if( array_search( $usuario->pivot->apodo, $sinApuesta ) !== false && $usuario->pivot->vidas_restantes)
                                                                    	        <option value="{{ $usuario->id.'-'.$usuario->pivot->apodo }}">{{ $usuario->pivot->apodo }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="submit" class="btn btn-primary" value="Escoger Equipo">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table   class="table table-text table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Vidas</th>
                                                        <th>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Usuario
                                                        </th>
                                                        @foreach($torneo->jornadaTorneo as $jornada1)
                                                            <th>{{ $jornada1->clave_jornada }}</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($torneo->usuarios as $numero => $participante)
                                                    
                                                    <tr>
                                                        <td>{{$numero + 1}}</td>
                                                        <td class="vidas{{$participante->pivot->vidas_restantes}}">{{$participante->pivot->vidas_restantes}}</td>
                                                        <td>{{$participante->pivot->apodo}}</td>
                                                        
                                                        @foreach($participante->jornadasApodo($participante->pivot->apodo)->get() as $jornada)
                                                            <td class="{{$jornada->pivot->resultado}}">
                                                                @if($jornada->participacion)
                                                                    @if($jornada->pivot->usuario_id == $usuarioId)
                                                                        <img style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                    @else
                                                                        <img style=" width: 50%;"  class="participantes_logo_equipo" src="{{asset('images/pick_escondido.png')}}"></img>
                                                                    @endif
                                                                @else
                                                                    <img  style=" width: 70%;"  class="participantes_logo_equipo" src="{{asset('images/equipos/'.$jornada->pivot->equipo_logo)}}"></img>
                                                                @endif
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
        jQuery(document).ready(function($) {
        
            var fecha = $('#fecha_inicio').val();
            var hora = $('#hora_inicio').val();
            var fecha1 = fecha.split("-");
            var hora1 = hora.split(":");
            var fechaevento = new Date(fecha1[0], fecha1[1]-1, fecha1[2], hora1[0], hora1[1], 00)
            $('#countdown-1').dsCountDown({
                endDate: fechaevento,
  				titleDays: 'Días',
  				titleHours: 'Horas',
  				titleMinutes: 'Min',
  				titleSeconds: 'Seg'
            });
            
            
            var fecha2 = $('#fecha_inicio_2').val();
            var hora2 = $('#hora_inicio_2').val();
            var fecha2 = fecha2.split("-");
            var hora2 = hora2.split(":");
            var fechaevento2 = new Date(fecha2[0], fecha2[1]-1, fecha2[2], hora2[0], hora2[1], 00)
            $('#countdown-2').dsCountDown({
                endDate: fechaevento2,
  				titleDays: 'Días',
  				titleHours: 'Horas',
  				titleMinutes: 'Min',
  				titleSeconds: 'Seg'
            });
            
        });
</script>
@endsection