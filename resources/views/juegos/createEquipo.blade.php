@extends('layouts.principal2')
@section('pageTitle', 'Apostar Partido')
@section('content')
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                            <div class="panel-heading">
                                <h3 class="panel-title panel-title-color">Partido</h3>
                            </div>
                            <div class="panel-body">
                            {!! Form::open(array('route' => array('juegos.storeEquipo',$partido->id),'method'=>'get','files'=> true)) !!}
                                <table class="table table-fixed table-text2">
                                    <thead>
                                        <th>Equipo Local</th>
                                        <th>Equipo Visitante</th>
                                    </thead>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('images/equipos/'.$equipo_local->escudo) }}" style=" width: 45%;">
                                            <div class="radio radio-success">
                                                        <input type="radio" name="equipo_seleccionado" id="radio1" value="{{$equipo_local->id}}">
                                                        <label for="radio1">
                                                            {{$equipo_local->equipo}}
                                                        </label>
                                            </div>
                                        </td>
                                        <td>
                                            <img src="{{ asset('images/equipos/'.$equipo_visitante->escudo) }}" style=" width: 45%;">
                                            <div class="radio radio-success">
                                                        <input type="radio" name="equipo_seleccionado" id="radio2" value="{{$equipo_visitante->id}}">
                                                        <label for="radio2">
                                                            {{$equipo_visitante->equipo}}
                                                        </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <br>
                                        <input type="hidden" name="usuario_id" value="{{currentUserid()}}"/>
                                        <input type="hidden" name="encuentro_id" value="{{$partido->id}}"/>
                                        {{ Form::submit('Registrar',array('class'=>'btn btn-primary waves-effect waves-light','id'=>'btnValidar'))}}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<script>
$("#btnValidar").click(function(event) {
  if(!$("input[name='equipo_seleccionado']").is(':checked')){
  	alert('Favor de seleccionar una opción');
  	return false;
  }
});
</script>
@endsection