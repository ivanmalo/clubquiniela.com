@extends('layouts.admin')
@section('pageTitle', 'Partidos')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
           
            <h1 class="panel-title panel-title-color">Editar Partido</h1>
          </div>
        </div>
    </div>
</div>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                    <div class="row">
                        <div class="col-sm-2 col-sm-push-10">
                            <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                        @if($tipo=='partido')
                    {{ Form::model($partido, array('route' => array('partidos.updateEquipo', $jornada->id), 'method' => 'get', 'files'=> true)) }}
                                <div class="row">
                                     <div class="col-md-12">
                                     <div class="form-group">
                                         <div class="col-sm-6">
                                        {{Form::label('equipo_local_id', 'Equipo Local', array('class' => 'awesome'))}}
                                        <select name="equipo_local_id" class="sel1 form-control">
                                                        
                                                        @foreach($encuentros as $encuentro)
                                                        <option <?php if($encuentro->equipoLocal->id==$partido->equipo_local_id){ echo 'selected="selected"'; } ?> value="{{$partido->equipo_local_id}}">{{$encuentro->equipoLocal->equipo}}</option>
                                                        @endforeach
                                        </select>
                                        </div>
                                         <div class="col-sm-6">
                                        {{Form::label('equipo_visitante_id', 'Equipo Visitante', array('class' => 'awesome'))}}
                                        <select name="equipo_visitante_id" class="sel2 form-control">
                                                       
                                                        @foreach($encuentros as $encuentro)
                                                        <option <?php if($encuentro->equipoVisitante->id==$partido->equipo_visitante_id){ echo 'selected="selected"'; } ?> value="{{$partido->equipo_visitante_id}}">{{$encuentro->equipoVisitante->equipo}}</option>
                                                        @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    </div>
                                 </div>
                               <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label>Fecha de Inicio</label>
                                                        <div>
                                                            <div class="input-group">
                                                                <input type="text" name="fecha_inicio" class="form-control" value="{{$partido->fecha_inicio}}" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                                            </div><!-- input-group -->
                                                        </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Hora de Inicio</label>
                                                    <div class="input-group clockpicker m-b-20" data-placement="top" data-align="top" data-autoclose="true">
                                                        <input type="text" class="form-control" name="hora_inicio" value="{{$partido->hora_inicio}}">
                                                        <span class="input-group-addon"> <span class="mdi mdi-clock"></span> </span>
                                                    </div>
                                                </div>
                                                        
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <br>
                                        {{ Form::hidden('tipo','partido',['class' => 'validate'])}}
                                        {{ Form::hidden('partido_id',$partido->id,['class' => 'validate'])}}
                                        {{ Form::hidden('torneo_id',$torneo_id,['class' => 'validate'])}}
                                        {{ Form::submit('Actualizar',array('class'=>'btn btn-primary waves-effect waves-light'))}}
                                    </div>
                                </div>
                    {{ Form::close() }}
                        @endif
                        @if($tipo=='puntos')
                                        {{ Form::model($partido, array('route' => array('partidos.updateEquipo', $jornada->id), 'method' => 'get', 'files'=> true)) }}
                                <div class="row">
                                     <div class="col-md-12">
                                     <div class="form-group">
                                         <div class="col-sm-6">
                                        {{Form::label('equipo_local_id', 'Equipo Local', array('class' => 'awesome'))}}
                                        {{Form::text('equipo_local',$partido->equipoLocal->equipo,array('class'=>'form-control','disabled'))}}
                                        {{Form::hidden('equipo_local_id',$partido->equipoLocal->id,['class' => 'validate'])}}
                                        </div>
                                         <div class="col-sm-6">
                                        {{Form::label('equipo_visitante_id', 'Equipo Visitante', array('class' => 'awesome'))}}
                                        {{Form::text('equipo_visitante',$partido->equipoVisitante->equipo,array('class'=>'form-control','disabled'))}}
                                        {{Form::hidden('equipo_visitante_id',$partido->equipoVisitante->id,['class' => 'validate'])}}
                                        </div>
                                    </div>
                                    </div>
                                 </div>
                               <div class="form-group">
                                                <div class="col-sm-6">
                                        {{Form::label('puntos_equipo_local', 'Puntos Equipo Local', array('class' => 'awesome'))}}
                                        {{Form::number('puntos_equipo_local',null,array('class'=>'form-control'))}}
                                                </div>
                                                <div class="col-sm-6">
                                        {{Form::label('puntos_equipo_visitante', 'Puntos Equipo Visitante', array('class' => 'awesome'))}}
                                        {{Form::number('puntos_equipo_visitante',null,array('class'=>'form-control'))}}
                                                </div>
                                                        
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <br>
                                        {{ Form::hidden('tipo','puntos',['class' => 'validate'])}}
                                        {{ Form::hidden('torneo_id',$torneo_id,['class' => 'validate'])}}
                                        {{ Form::hidden('partido_id',$partido->id,['class' => 'validate'])}}
                                        {{ Form::submit('Actualizar',array('class'=>'btn btn-primary waves-effect waves-light'))}}
                                    </div>
                                </div>
                    {{ Form::close() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).on('change','.sel1',function(){
      var opcion = $(this).val();
      $(".sel2").each(function(){
        $('.sel2 option').removeAttr('disabled');
        });
      $('.sel2').each(function(){
          $('.sel2 option[value="'+opcion+'"]').attr('disabled','disabled');
      })
    });
    
    $(document).on('change','.sel2',function(){
      var opcion = $(this).val();
      $(".sel1").each(function(){
        $('.sel1 option').removeAttr('disabled');
        });
      $('.sel1').each(function(){
          $('.sel1 option[value="'+opcion+'"]').attr('disabled','disabled');
      })
    });
    
          
</script>
@endsection