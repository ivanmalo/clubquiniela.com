@extends('layouts.admin')
@section('pageTitle', 'Partidos')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
           
            <h1 class="panel-title panel-title-color">Crear Partido</h1>
          </div>
        </div>
    </div>
</div>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                    <div class="row">
                        <div class="col-sm-2 col-sm-push-10">
                            <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                         <form action="{{ route('partidos.storePartido',[$jornada->id]) }}">
                                            <div class="form-group">
                                                
                                                <div class="col-sm-6">
                                                    <label class="control-label">Equipo Local</label>
                                                    <select name="equipo_local_id" class="sel1 form-control">
                                                        <option selected="selected">Seleccionar</option>
                                                        @foreach($equipos as $equipo)
                                                        <option value="{{$equipo->id}}">{{$equipo->equipo}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label">Equipo Visitante</label>
                                                    <select name="equipo_visitante_id" class="sel2 form-control">
                                                        <option selected="selected">Seleccionar</option>
                                                        @foreach($equipos as $equipo)
                                                        <option value="{{$equipo->id}}">{{$equipo->equipo}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label>Fecha de Inicio</label>
                                                        <div>
                                                            <div class="input-group">
                                                                <input type="text" name="fecha_inicio" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                                            </div><!-- input-group -->
                                                        </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Hora de Inicio</label>
                                                    <div class="input-group clockpicker m-b-20" data-placement="top" data-align="top" data-autoclose="true">
                                                        <input type="text" class="form-control" name="hora_inicio">
                                                        <span class="input-group-addon"> <span class="mdi mdi-clock"></span> </span>
                                                    </div>
                                                </div>
                                                        
                                            </div>
                                            
                                            <div class="form-group  text-right m-b-0">
                                                <div class="col-sm-12">
                                                    <br>
                                                    <input type="hidden" name="torneo_id" value="{{$torneo->id}}"/>
                                                    <input type="hidden" name="jornada_id" value="{{$jornada->id}}" />
                                                    <input type="hidden" name="numero_vidas" value="0"/>
                                                    <input type="hidden" name="ganador" value="0"/>
                                                    <input type="hidden" name="perdedor" value="0"/>
                                                    <input type="hidden" name="puntos_equipo_local" value="0"/>
                                                    <input type="hidden" name="puntos_equipo_visitante" value="0"/>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                                Registrar
                                                    </button>
                                                </div>
                                            </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).on('change','.sel1',function(){
      var opcion = $(this).val();
      $(".sel2").each(function(){
        $('.sel2 option').removeAttr('disabled');
        });
      $('.sel2').each(function(){
          $('.sel2 option[value="'+opcion+'"]').attr('disabled','disabled');
      })
    });
    
    $(document).on('change','.sel2',function(){
      var opcion = $(this).val();
      $(".sel1").each(function(){
        $('.sel1 option').removeAttr('disabled');
        });
      $('.sel1').each(function(){
          $('.sel1 option[value="'+opcion+'"]').attr('disabled','disabled');
      })
    });
    
          
</script>


@endsection