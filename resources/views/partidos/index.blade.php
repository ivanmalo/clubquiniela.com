@extends('layouts.admin')
@section('pageTitle', 'Calendario de Partidos')
@section('content')

        
        <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                          <div class="panel-heading">
                           
                            <h3 class="panel-title panel-title-color">Partidos</h3>
                          </div>
                          <div class="panel-body">
                               <div class="row">
                                    <div class="col-sm-6 col-sm-push-8">
                                        
                                        
                                        <div class="col-lg-5">
                                             <form action="{{route('partidos.createPartidos',[$torneo->id])}}">
                                                <input type="hidden" name="jornada_id" value="{{$jornada->id}}" />
                                                <input class="btn btn-primary" type="submit" value="Nuevo Partido"/>
                                            </form>
                                        </div>
                                        <div class="col-lg-4">
                                            <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(count($encuentros)>0)
                                        <table class="table table-text table-striped table-bordered">
                                            <thead>
                                                <th>Fecha</th>
                                                <th>Hora</th>
                                                <th>Equipo Local</th>
                                                <th>Equipo Visitante</th>
                                                <th>Acción</th>
                                            </thead>
                                            <tbody>
                                                @foreach($encuentros as $encuentro)
                                                <tr>
                                                    <td>{{$encuentro->fecha_inicio}}</td>
                                                    <td>{{$encuentro->hora_inicio}}</td>
                                                    <td>
                                                        {{$encuentro->equipoLocal->equipo}}
                                                        <br>
                                                        <img src="{{ asset('images/equipos/'.$encuentro->equipoLocal->escudo) }}" style=" width: 17%;">
                                                        <br>
                                                        <h4>{{$encuentro->puntos_equipo_local}}</h4>
                                                    </td>
                                                    <td>
                                                        {{$encuentro->equipoVisitante->equipo}}
                                                        <br>
                                                        <img src="{{ asset('images/equipos/'.$encuentro->equipoVisitante->escudo) }}" style=" width: 17%;">
                                                        <br>
                                                        <h4>{{$encuentro->puntos_equipo_visitante}}</h4>
                                                    </td>
                                                    
                                                    <td>
                                                        
                                                        <div class="col-lg-3">
                                                        <form action="{{route('partidos.editPartido',[$encuentro->jornada_id])}}">
                                                            <input type="hidden" name="tipo" value="partido"/>
                                                            <input type="hidden" name="id_partido" value="{{$encuentro->id}}" />
                                                            <input type="hidden" name="torneo_id" value="{{$torneo->id}}"/>
                                                            <input class="btn btn-primary" type="submit" value="Editar"/>
                                                        </form>
                                                        </div>
                                                        <div class="col-lg-5">
                                                        <form action="{{route('partidos.editPartido',[$encuentro->jornada_id])}}">
                                                            <input type="hidden" name="tipo" value="puntos"/>
                                                            <input type="hidden" name="id_partido" value="{{$encuentro->id}}" />
                                                            <input type="hidden" name="torneo_id" value="{{$torneo->id}}"/>
                                                            <input class="btn btn-custom" type="submit" value="Puntuación"/>
                                                        </form>
                                                        </div>
                                                        
                                                        <div class="col-lg-3">
                                                        <form action="{{route('partidos.eliminarPartido',[$torneo->id])}}">
                                                            <input type="hidden" name="id_partido" value="{{$encuentro->id}}" />
                                                            <input type="hidden" name="jornada_id" value="{{$jornada->id}}" />
                                                            <input class="btn btn-danger" type="submit" value="Eliminar"/>
                                                        </form>
                                                        </div>
                                                        
                                                    </td>
                                                    
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                        @else
                                        <h3>No hay partidos registros</h3>
                                        @endif
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                
                <br />
                
            </div>
        </div>
    </div>
@endsection