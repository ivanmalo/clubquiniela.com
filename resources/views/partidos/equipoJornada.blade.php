@extends('layouts.principal2')
@section('pageTitle', 'Seleccionar Equipo')
@section('content')
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Torneo: {{ $torneo->nombre}}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                            <div class="panel-heading">
                                <h3 class="panel-title panel-title-color">{{ $jornada->clave_jornada }}: Elige a un equipo para participar</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div style="text-align: center;" class="col-5 col-sm-5 col-md-5 col-xs-5 panel-body">
                                        <h3 class="panel-title"> Equipo Local </h3>
                                    </div>
                                    
                                    <div style="text-align: center;" class="col-2 col-sm-2 col-md-2 col-xs-2 panel-body">
                                    </div>
                                    
                                    <div style="text-align: center;" class="col-5 col-sm-5 col-md-5 col-xs-5 panel-body">
                                        <h3 class="panel-title"> Equipo Visitante </h3>
                                    </div>
                                </div>
                                @foreach($encuentros as $encuentro)
                                    
                                    <div class="row">
                                        <div style="text-align: center;" class="col-5 col-sm-5 col-md-5 col-xs-5 panel-body">
                                            
                                            {!! Form::open(array('route' => array('juegos.storeEquipo',$jornada->id),'method'=>'get','id' => 'btn-success'.$encuentro->equipoLocal->id,'files'=> true)) !!}
                                                <input type="hidden" name="apodo" value="{{$apodo}}"/>
                                                <input type="hidden" name="usuario_id" value="{{$usuarioId}}"/>
                                                <input type="hidden" name="jornada_id" value="{{$jornada->id}}"/>
                                                <input type="hidden" name="equipo_seleccionado" value="{{$encuentro->equipoLocal->id}}"/>
                                                <input type="hidden" name="equipo_logo" value="{{$encuentro->equipoLocal->escudo}}"/>
                                                
                                                @if(array_search( $encuentro->equipo_local_id, $equiposUsados ) !== false)
                                                    <div class="btn">
                                                    	<img src="{{ asset('images/equipos/'.$encuentro->equipoLocal->escudo) }}" style=" width: 100%;">
                                                    </div>                                                                	                             
                                                @else
                                                    <button type="submit" class="btn red success btn-default waves-effect waves-light" id-btn = '{{$encuentro->equipoLocal->id}}'>
                                                        <img src="{{ asset('images/equipos/'.$encuentro->equipoLocal->escudo) }}" style=" width: 100%;">
                                                    </button>
                                                @endif
                                                
                                             {{ Form::close() }}
                                        </div>
                                        
                                        <div style="text-align: center;" class="col-2 col-sm-2 col-md-2 col-xs-2 panel-body">
                                            <br>
                                            <br>
                                            <h4>VS</h4>
                                            <br>
                                            <br>
                                        </div>
                                        
                                        <div style="text-align: center;" class="col-5 col-sm-5 col-md-5 col-xs-5 panel-body">
                                            {!! Form::open(array('route' => array('juegos.storeEquipo',$jornada->id),'method'=>'get','id' => 'btn-success'.$encuentro->equipoVisitante->id,'files'=> true)) !!}
                                                <input type="hidden" name="apodo" value="{{$apodo}}"/>
                                                <input type="hidden" name="usuario_id" value="{{$usuarioId}}"/>
                                                <input type="hidden" name="jornada_id" value="{{$jornada->id}}"/>
                                                <input type="hidden" name="equipo_seleccionado" value="{{$encuentro->equipoVisitante->id}}"/>
                                                <input type="hidden" name="equipo_logo" value="{{$encuentro->equipoVisitante->escudo}}"/>
                                                @if(array_search( $encuentro->equipo_visitante_id, $equiposUsados ) !== false)
                                                    <div class="btn">
                                                    	<img src="{{ asset('images/equipos/'.$encuentro->equipoVisitante->escudo) }}" style=" width: 100%;">
                                                    </div>                                                                	                             
                                                @else
                                                    <div type="submit" class="btn red success btn-default waves-effect waves-light" id-btn = '{{$encuentro->equipoVisitante->id}}'>
                                                        <img src="{{ asset('images/equipos/'.$encuentro->equipoVisitante->escudo) }}" style=" width: 100%;">
                                                    </div>
                                                @endif
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<meta http-equiv="Expires" content="0" /> 
<meta http-equiv="Pragma" content="no-cache" />

<script type="text/javascript">
  if(history.forward(1)){
    location.replace( history.forward(1) );
  }
</script>

@endsection

