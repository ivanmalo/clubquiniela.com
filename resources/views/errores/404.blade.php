@extends('layouts.registro')
@section('pageTitle', 'Error 404')
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 card-box">
                    <div class="text-center">
                        <h2 class="text-uppercase m-t-0 m-b-30">
                            <a href="index.html" class="text-success">
                                <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                
                            </a>
                        </h2>
                        <h4 class="text-uppercase font-bold m-b-0">No se encontro la pagina seleccionada.</h4>
                    </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection