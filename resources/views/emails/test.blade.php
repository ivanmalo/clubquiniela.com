@component('mail::message')

# Notificación de Resultados

Estimado {{$usuario->name}} te informamos que los equipos seleccionados con tus usuarios *{{$usuario->apodos}}* han perdido en la jornada 1 y se les resto una vida,
entra a <a href="https://clubquiniela.com">Club Quiniela</a> para ver tu estatus.

Gracias.
<br>

@endcomponent
