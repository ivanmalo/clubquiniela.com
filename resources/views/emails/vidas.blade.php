@component('mail::message')
# Notificación de Resultados

Estimado **{{$user}}** te informamos que tus equipos seleccionados con los apodos **{{$apodos}}** han perdido dentro de la {{$jornada}} - {{$torneo}} y se les restara una vida,
entra a ClubQuiniela 2018 para ver tu status.



Gracias,<br>
{{ config('app.name') }}
@endcomponent
