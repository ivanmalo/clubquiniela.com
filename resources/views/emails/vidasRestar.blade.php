@component('mail::message')
# Notificación de Equipo

The body of your message.

Estimado **{{$user}}** te informamos que tu usuario con los apodo(s) **{{$apodos}}** no seleccionaron un equipo para la {{$jornada}} - {{$torneo}}, y se les restara una vida,
entra a ClubQuiniela 2018 para ver tu status.

Gracias,<br>
{{ config('app.name') }}
@endcomponent
