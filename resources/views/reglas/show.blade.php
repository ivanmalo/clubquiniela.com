@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-80 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <a href="javascript:history.back(1)">
                                        Regresar
                                    </a>
                                </div>
                                
                            <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">{{' ' . $regla->nombre  }} <img src="@if($regla->nombre =='Reglas NFL') {{asset('images/log_nfl.jpg')}} @else {{asset('images/ligamx.png')}}   @endif" width="50"></img></h4>
                            </div>
                            
                            <div class="col-sm-12">
                                <br>
                                <br>
                            </div>
                            
                            <div class="panel-body">
                                
                                <div class="account-content">
                                    <?php echo $regla->contenido; ?>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
