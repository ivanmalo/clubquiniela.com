@extends('layouts.admin')
@section('pageTitle', 'Crear Equipo')
@section('content')

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
    <div class="col-md-12">
        <div class="panel panel-personalizado">
          <div class="panel-heading">
            <h3 class="panel-title panel-title-color">Nuevo Equipo</h3>
          </div>
          <div class="panel-body">
              <div class="row">
                    <div class="col-sm-4 col-sm-push-10">
                        <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    {!! Form::open(array('route' => 'equipos.store','method'=>'POST','files'=> true)) !!}
                                <div class="row">
                                     <div class="col-md-12">
                                     <div class="form-group">
                                        {{Form::label('equipo', 'Nombre del Equipo', array('class' => 'awesome'))}}
                                        {{ Form::text('equipo',null,['class' => 'form-control validate'])}}
                                    </div>
                                    </div>
                                 </div>
                               <div class="form-group m-b-0">
                                            {{Form::label('escudo', 'Seleccionar Escudo', array('class' => 'control-label awesome'))}}
                                            {{ Form::file('escudo',null,['class' => 'filestyle validate','data-buttonname'=>'btn-primary'])}}
                                            
                                </div>
                                <div class="form-group m-b-0">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <br>
                                        {{ Form::submit('Registrar',array('class'=>'btn btn-primary waves-effect waves-light'))}}
                                    </div>
                                </div>
                    {{ Form::close() }}
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
                
            </div>
        </div>
    </div>
@endsection