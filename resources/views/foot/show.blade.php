@extends('layouts.admin')
@section('pageTitle', 'Calendario de Partidos')
@section('content')


        <div class="panel panel-default">
            <br />
            <div class="row">
                <div class="col-md-1 col-md-push-8">
                    
                    <button type="button" <?php if(count($arrayResponse)>0){}else{ echo "disabled";} ?> class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dar de alta a la Liga.">Aprobra Liga</button>
                </div>
                <div class="col-md-2 col-md-push-9">
                    <input type="button" class="btn btn-danger " onclick="history.back()" name="volver atrás" value="Regresar">
                        
                </div>
                
            </div>
            <div class="panel-body">
                @if(count($arrayResponse)>0)
                <div class="row">
                    
                </div>
                <table class="table">
                    <thead>
                        <th>
                            Fecha Inicio
                        </th>
                        <th>
                            Hora Incio
                        </th>
                        <th>Equipo Local</th>
                        <th>Equipo Visitante</th>
                        <th>Lugar de Encuentro</th>
                        
                    </thead>
                    <tbody>
    @foreach ( $arrayResponse as $array)
                    <tr>
                        <td>
                             {{$array['date']}}
                        </td>
                        <td>
                             {{$array['time']}}
                        </td>
                        <td>
                             {{$array['home_name']}}
                        </td>
                        <td>
                             {{$array['away_name']}}
                        </td>
                        <td>
                             {{$array['location']}}
                        </td>
                        
                    </tr>
    @endforeach
                    </tbody>
                </table>
               @else
               <h3>No hay encuentros disponibles.</h3>
               @endif
            </div>
        </div>
@endsection