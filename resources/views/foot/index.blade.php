@extends('layouts.admin')
@section('pageTitle', 'Principal')
@section('content')

        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th>
                            Liga
                        </th>
                        <th>
                            País
                        </th>
                        <th></th>
                    </thead>
                    <tbody>
    @foreach ( $arrayResponse as $array)
                    <tr>
                        <td style="width:40%;">
                             {{$array['league_name']}}
                        </td>
                        <td style="width:20%;">
                             {{$array['country_name']}}
                        </td>
                        <td style="width:30%;">
                            <a href="/ligas/{{$array['league_id']}}"  class="btn btn-warning">
                                Calendario
                            </a>
                        </td>
                    </tr>
    @endforeach
                    </tbody>
                </table>
               
            </div>
        </div>
@endsection