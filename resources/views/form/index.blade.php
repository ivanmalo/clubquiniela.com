@extends('layouts.principal2')

@section('pageTitle', 'Subir archivo')

@section('content')
    @include('mensajes/alertas')
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-personalizado">
                            <div class="panel-heading">
                                <h3 class="panel-title panel-title-color">Torneo Mundial</h3>
                            </div>
                            <div class="panel-body">
                            
                            <div class="col-md-12 panel panel-personalizado">    
                                <div class="col-sm-12">
                                    <h4>Ver Reglas del juego: <a href="{{ route('instructivo') }}" target="blanck_">Aquí</a> </h4>
                                </div>
                                
                                <div class="col-sm-12">
                                    <h4> Descargar archivo para participar:  <a class="btn btn-default waves-effect waves-light" href="{{asset('archivos/Rusia2018-Fixture.xlsm')}}" download="Rusia 2018 Fixture.xlsm">Descargar</a> </h4>
                                </div>
                                <br><br>
                            </div>
                                
                            @if($participaciones->count())
                                @foreach($participaciones as $participacion)
                                    <div class="col-md-12 panel panel-personalizado">
                                        @if( $participacion->archivo ) 
                                            <br><p>Participación {{$indice++}} </p> <br>
                                            {{ Form::model($participacion->archivo, array('route' => array('archivos.update',$participacion->archivo->id), 'method' => 'PUT', 'files'=> true)) }}
                                                {{ csrf_field() }}
                                                {{ Form::hidden('archivo_id', $participacion->archivo->id )}}
                                                {{ Form::hidden('torneo_id', $participacion->pivot->id )}}
                                                <div class=" col-sm-12 col-md-4">
                                                     <div class="form-group">
                                                        {{Form::label('apodo', 'Apodo', array('class' => 'awesome'))}}
                                                        {{ Form::text('apodo', $participacion->archivo->apodo, ['class' => 'form-control validate'])}}
                                                    </div>
                                                </div>
                                                <div class=" col-sm-12 col-md-4 ">
                                                    <div class="form-group">
                                                        {{Form::label('archivo', 'Cambiar quiniela', array('class' => 'awesome'))}}
                                                         <input name="archivo" type="file" class="form-control-file" />
                                                       
                                                    </div>
                                                    
                                                     
                                                </div>
                                                
                                                <div class=" col-sm-12 col-md-4">
                                                    
                                                     {{Form::label('archivo', 'Descarga tu quiniela', array('class' => 'pull-right'))}}
                                                    <div class="form-group pull-right">
                                                       
                                                        <a class="btn btn-primary waves-effect waves-light" href="{{ $participacion->archivo->ruta }}" download="ClubquinielaMundial.xlsm">Descargar Archivo</a>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" value="Actualizar" class="btn btn-primary waves-effect waves-light"/>
                                                </div>
                                            {{ Form::close() }}
                                        @else
                                            <br><p>Participación {{$indice++}}: Pendiente subir archivo</p> <br>
                                            <form action='/storagefile' method='post' enctype='multipart/form-data'>
                                                {{ csrf_field() }}
                                                
                                                {{ Form::hidden('torneo_id', $participacion->pivot->id )}}
                                                <div class="col-md-6">
                                                     <div class="form-group">
                                                        {{Form::label('apodo', 'Apodo', array('class' => 'awesome'))}}
                                                        {{ Form::text('apodo', $participacion->pivot->apodo, ['class' => 'form-control validate'])}}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                     <div class="form-group">
                                                        {{Form::label('file', 'Subir archivo', array('class' => 'awesome'))}}
                                                        <input name="file" type="file" class="form-control-file" />
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" value="Enviar" class="btn btn-primary waves-effect waves-light"/>
                                                </div>
                                            </form>
                                        @endif
                                        <div class="col-md-12">
                                            <br><br>
                                        </div>
                                        
                                    </div>
                                @endforeach
                            @else
                                <div class="col-sm-12">
                                    <p>
                                        Te invitamos al nuevo torneo mundial de Russia. <a href="{{ route('participantes.comprarEntrada') }}">Comprar entradas</a>
                                    </p>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection