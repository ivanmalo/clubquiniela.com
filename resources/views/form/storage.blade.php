@extends('layouts.principal2')
@section('pageTitle', 'Upload file')
@section('content')
  
   <div class="row">
       <div class="col-md-12">
           <h1>Se han guardado con exito los datos</h1>
           <a href="{{$linkfile}}" download="archivo.xlsm"> descagar archivo</a>
       </div>
   </div>

@endsection