                <aside class="sidebar-navigation">
                    <div class="scrollbar-wrapper">
                        <div>
                            <button type="button" class="button-menu-mobile btn-mobile-view visible-xs visible-sm">
                                <i class="mdi mdi-close"></i>
                            </button>
                            <!-- User Detail box -->
                            <div class="user-details">
                                <div class="pull-left">
                                    <img src="{{ asset('images/users/avatar-1.png') }}" alt="" class="thumb-md img-circle">
                                    
                                </div>
                                <div class="user-info">
                                    <a href="{{ route('participantes.userPerfil') }}">{{currentUser()->name}}</a>
                                    <p class="text-muted m-0">Administrador</p>
                                </div>
                            </div>
                            <!--- End User Detail box -->

                            <!-- Left Menu Start -->
                            <ul class="metisMenu nav" id="side-menu">
                                <li><a href="{{ route('torneo.principal') }}"><i class="fa fa-plus-square" aria-hidden="true"></i> Torneos </a></li>
                                <li><a href="{{ route('equipos.index') }}"><i class="mdi mdi-animation"></i> Equipos </a></li>
                                <li><a href="{{ route('juegos.index') }}"><i class="ti-home"></i> Escritorio </a></li>

                                <li><a href="{{ route('participantes.juegoReglas') }}"><i class="mdi mdi-ruler"></i> Reglas Juego </a></li>

                                <li><a href="{{ route('participantes.userPerfil') }}"><i class="mdi mdi-account-outline"></i>Perfil </a></li>
                                {{--
                                <li><a href="{{ route('participantes.comprarEntrada') }}"><i class="ti-money"></i>Comprar Entrada</a></li>
                                <li><a href="{{ route('participantes.juegoParticipantes') }}"><i class="ti-calendar"></i>Jornadas</a></li>
                                <li><a href="{{ route('participantes.comprarEntradaRevancha') }}"><i class="ti-money"></i>Comprar Torneo Revancha</a></li>
                                --}}
                            </ul>
                        </div>
                    </div><!--Scrollbar wrapper-->
                </aside>