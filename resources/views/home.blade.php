@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <a href="{{ route('participantes') }}">
                                        Participantes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="{{ route('reglas') }}">
                                        Reglas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                                
                            <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">Elige una suscripción</h4>
                            </div>
                            
                            <div class="col-sm-12">
                                <br>
                                <br>
                            </div>
                            
                            <div class="panel-body">
                                <div class="account-content">
                                    
                                    <div style="width:100%; text-align: center;"  class="countdown center-block"></div>
                                        
                                        <div class="col-12 col-sm-6 col-md-6" role="group" aria-label="...">
                                            <div class="text-center">
                                                <h5 class="text-uppercase font-bold m-b-0">Torneo 250 $250.00 MXN</h5>
                                                <br>
                                            </div>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="5DMDJT27ZNS2S">
                                                
                                                <div class="form-group">
                                                    <input type="hidden" name="on0" value="Tu correo">
                                                    <label class="control-label">Tu correo:</label>
                                                    <input type="text" value="{{$correo}}" readonly name="os0" maxlength="200" class="form-control">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="hidden" name="on1" value="Tu nombre">
                                                    <label class="control-label">Tu nombre:</label>
                                                    <input type="text" value="{{$nombre}}" readonly name="os1" maxlength="200" class="form-control">
                                                </div>
                                                
                                                <div class="form-group" style="text-align: center;">
                                                    <input type="image" src="https://www.paypalobjects.com/en_US/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                </div>
                                            </form>
                                        </div>
                                        
                                        <div class="col-12 col-sm-6 col-md-6" role="group" aria-label="...">
                                            <div class="text-center">
                                                <h5 class="text-uppercase font-bold m-b-0">Torneo 500 $500.00 MXN</h5>
                                                <br>
                                            </div>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="FFMC8ZJ5KJSXJ">
                                                
                                                <div class="form-group">
                                                    <input type="hidden" name="on0" value="Tu correo">
                                                    <label class="control-label">Tu correo:</label>
                                                    <input type="text" value="{{$correo}}" readonly name="os0" maxlength="200" class="form-control">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="hidden" name="on1" value="Tu nombre">
                                                    <label class="control-label">Tu nombre:</label>
                                                    <input type="text" value="{{$nombre}}" readonly name="os1" maxlength="200" class="form-control">
                                                </div>
                                                
                                                <div class="form-group" style="text-align: center;">
                                                    <input type="image" src="https://www.paypalobjects.com/en_US/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
