@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <a href="{{ route('reglas') }}">
                                        Reglas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </a>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                                
                            <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">Elige una opción</h4>
                            </div>
                            
                            <div class="col-sm-12">
                                <br>
                                <br>
                            </div>
                            
                            <div class="panel-body">
                                <div class="account-content">
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                    <input type="hidden" name="cmd" value="_s-xclick">
                                    <input type="hidden" name="hosted_button_id" value="SSB62PCPXUURW">
                                    <table>
                                    <tr><td><input type="hidden" name="on0" value="Tu correo">Tu correo</td></tr><tr><td><input type="text" name="os0" maxlength="200"></td></tr>
                                    <tr><td><input type="hidden" name="on1" value="Tu nombre">Tu nombre</td></tr><tr><td><input type="text" name="os1" maxlength="200"></td></tr>
                                    </table>
                                    <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
                                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                    {{--
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="Z528RGUJGZZJN">
                                        <table style="width:100%;">
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="on0" value="Torneos Disponibles">
                                                        <label class="control-label">Torneos Disponibles</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control" name="os0">
                                                            <option value="Torneo 250">Torneo 250 $250.00 MXN</option>
                                                            <option value="Torneo 500">Torneo 500 $500.00 MXN</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="on1" value="Tu correo">
                                                        <label class="control-label">Tu correo</label>
                                                        <input type="text" value="{{$correo}}" name="os1" class="form-control" maxlength="200" readonly>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="on2" value="Tu nombre">
                                                        <label class="control-label">Tu nombre</label>
                                                        <input type="text" value="{{$nombre}}" class="form-control" name="os2" maxlength="200" readonly>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <br >
                                        <div class="form-group" style="text-align: center;">
                                            <input type="hidden" name="currency_code" value="MXN">
                                            <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
                                        </div>
                                    </form>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
