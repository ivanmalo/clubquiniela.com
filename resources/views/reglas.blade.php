@extends('layouts.registro')
@section('pageTitle', 'Principal')
    @section('content')
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-80 card-box">
                                
                                <div class="col-sm-12 text-right">
                                    <a href="javascript:history.back(1)">
                                        Regresar
                                    </a>
                                </div>
                                
                            <div class="text-center">
                                <h2 class="text-uppercase m-t-0 m-b-30">
                                    <a href="index.html" class="text-success">
                                        <span><img src="{{ asset('images/logo.png') }}" alt="" height="120"></span>
                                        
                                    </a>
                                </h2>
                                <h4 class="text-uppercase font-bold m-b-0">Reglas</h4>
                            </div>
                            
                            <div class="col-sm-12">
                                <br>
                                <br>
                            </div>
                            
                            <div class="panel-body">
                                
                                <div class="account-content">
                            
                            <p>
                                Club quiniela
                            </p>

<p>
    IMPORTANTE: Nos reservamos el derecho de Admisión
En caso de que existan controversias no previstas en las reglas, será decisión irrevocable del comité organizador tomar la mejor decisión para el bienestar de todos los jugadores
Mecánica
</p>

<p>
    A través de nuestra página de Internet www.clubquiniela.com cada participante deberá escoger un equipo en cada jornada. Cada participante cuenta con 1 VIDA.  En caso de que dicho equipo resulte vencedor o bien empate, el participante seguirá con vida en el certamen; por el contrario si dicho equipo resulta perdedor, el participante perderá una vida y con ello fuera del torneo. 

</p>

<p>
    Los Participantes Sobrevivientes deberán seleccionar un nuevo equipo, diferente a los previamente escogidos, e ingresarlo dentro de la página. Es decir que no podrá votar por equipos previamente seleccionados. 
El día límite para ingresar a sus equipos en la página de Internet es de 30 minutos antes del primer partido de la jornada. En la página de Internet existe un Reloj, el cual se considera el "Reloj Oficial". 
IMPORTANTE: EN CASO DE NO REALIZAR SU SELECCION EN TIEMPO, EL PARTICIPANTE PERDERA LA VIDA AUTOMATICAMENTE SIN EXCEPCIÓN ALGUNA. 
ACLARACIÓN: Por seguridad de la información los participantes, los votos no se pueden cambiar bajo ninguna circunstancia. Les recordamos analizar bien sus votos ya que bajo ninguna excepción realizaremos cambios.
Como ayuda a nuestros Participantes, el sistema manda recordatorios vía correo electrónico, 24  horas antes de la jornada, a los Participantes que no han seleccionado equipo.
El Ganador o Ganadores serán aquellos Participantes que lleguen más lejos en el Certamen (Únicamente Temporada Regular).
El número máximo de Participaciones por TORNEO por persona es de 3. 

</p>
<p>
    El Premio a repartir será la Bolsa Acumulada de todas las Participaciones de Cada TORNEO, restando a esto el 10% de Gastos Administrativos.
IMPORTANTE: El sobreprecio por el uso de la PASARELA DE PAGO, no formarán parte de la  Bolsa a Repartir.
En caso de empate, dicho premio será dividido, en partes iguales, entre el número de Ganadores.
Una vez cerradas las inscripciones, el Comité Organizador dará a conocer el Monto de cada Bolsa.

</p>

<p>
    Agradeceremos efectuar el pago de las quinielas ya sea en efectivo, o a través de LA PASARELA DE PAGO encontrada en la pagina.
Aclaramos que toda aquella persona que no haya pagado por completo su participación por ningún motivo tendrá derecho a la misma.

</p>

<p>
    Los pagos a través de LA PASARELA DE PAGO son inmediatos, y los usuarios quedan habilitados después que se ha validado el pago. 
En el caso de los depósitos en efectivo, una vez recibido el pago se les habilitará como Participantes del Certamen de tal manera que puedan ingresar a nuestra Página de Internet y poder efectuar su respectivo voto.

</p>
<p>
    NOTAS IMPORTANTES:


</p>
<p>
    -NO PUEDES ELEGIR EQUIPOS DE UN PARTIDO PROGAMADO PARA OTRA FECHA QUE NO SEA LA JORNADA ORIGINAL.

</p>
<p>
    -NO PUEDES ELEGIR EQUIPOS DE UN PARTIDO PROGAMADO PARA OTRA FECHA QUE NO SEA LA JORNADA ORIGINAL.

</p>

<p>
    -SI EL PARTIDO ES CANCELADO DESPUES DEL SILBATAZO INICIAL, SE CONSIDERARA EL RESULTADO PARCIAL COMO MARCADOR FINAL. ELIMINANDO AL EQUIPO QUE VAYA PERDIENDO.

</p>

<p>
    -SEÑALAMOS QUE CADA TORNEO ES BOLSA INDEPENDIENTE

</p>



<p>
    TORNEO Quiniela, Fechas y Cuotas
</p>

<p>
    TORNEO de 500 (1 VIDA)
</p>

<p>
    1.	Este TORNEO inicia en la Jornada 1 de la Liga MX. El primer juego es el día 20 DE JULIO DEL 2018 A LAS 7PM, por lo que el límite de selección de equipos es 30 minutos antes de iniciar el partido.
2.	Cuenta con un cupo máximo de 500 participaciones.
3.	Cada usuario puede adquirir hasta 3 participaciones en este TORNEO
4.	El costo de cada participación es de $ 500.00 MXN.
5.	La fecha límite para cualquier inscripción a este TORNEO, será el día 20 de JULIO del 2018, a las 18:00 Horas ( 6:00 PM ). A partir de dicho momento, no es posible recibir más inscripciones

</p>
<p>
    TORNEO REVANCHA 2018 (1VIDA)

</p>
<p>
    1.	Este TORNEO inicia en la Jornada  7. El primer juego de esta Semana es el día 24 DE AGOSTO DEL 2018 A LAS 9PM por lo que el límite de selección de equipos es el día VIERNES 24 DE AGOSTO A LAS 8:30PM, 30 minutos antes del primer encuentro.
2.	Cuenta con un cupo máximo de 500 participaciones.
3.	Cada usuario puede adquirir hasta 3 participaciones en este TORNEO.
4.	El costo de cada participación es de $ 500.00 MXN.
5.	La fecha límite para cualquier inscripción a este TORNEO, será el dia VIERNES 24 DE AGOSTO A LAS 8:30PM). A partir de dicho momento, no será posible recibir más inscripciones

</p>

<p>
    Pagos

</p>
<p>
    1.	PASARELA DE PAGO: DAMOS INDICACIONES DE CÓMO HACERLO A TRAVES DE LA PAGINA WEB


</p>

                              
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
